#ifndef STARTWINDOW_H
#define STARTWINDOW_H

#include <QWidget>
#include <QVBoxLayout>
#include <QString>
#include <QSettings>

#include <QPushButton>

//main
#include "workwindow.h"

#include "sharedresources/configdata.h"
extern ConfigData *CFGD;

#include "sharedresources/dataqto.h"
extern DataQTO *MDE;

class StartWindow : public QWidget
{
    Q_OBJECT

public:
    StartWindow();
    static int const EXIT_CODE_REBOOT;
    ~StartWindow();

private:
    QSettings *settingsConnect;
    QTextCodec *codec = QTextCodec::codecForName("Windows-1251");

    QPushButton *okbut = new QPushButton("Принять");
    QPushButton *cancelbut = new QPushButton("Выйти");
    QLineEdit *nameConnect = new QLineEdit;
    QLineEdit *ip = new QLineEdit;
    QLineEdit *port = new QLineEdit;
    QLineEdit *base = new QLineEdit;
    QLineEdit *user = new QLineEdit;
    QLineEdit *password = new QLineEdit;

    QListWidget *listConnect;
    QPoint pt;

    QWidget *addWidget = nullptr;
    QLineEdit *enameConnect = new QLineEdit;
    QLineEdit *eip = new QLineEdit;
    QLineEdit *eport = new QLineEdit;
    QLineEdit *ebase = new QLineEdit;
    QLineEdit *euser = new QLineEdit;
    QLineEdit *epassword = new QLineEdit;

    void createWindowIN();

private slots:
    void okAction();
    void cancelAction();
    void getSettings(QListWidgetItem *item);
    void deleteConnect();
    void addNewConnect();
    void setSelection();
    void prepareMenu(const QPoint & pos);
    void editConnect();

    //addNew
    void okConnectAdd();
    void cancelConnectAdd();
};

#endif // STARTWINDOW_H
