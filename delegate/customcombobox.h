#ifndef CUSTOMCOMBOBOX_H
#define CUSTOMCOMBOBOX_H

#include <QComboBox>
#include <QObject>

class CustomComboBox : public QComboBox
{
    Q_OBJECT
public:

    struct colorRange
    {
        bool selected;
        QColor color;
    };

    CustomComboBox();

    QList<CustomComboBox::colorRange> colorRange =
    {
        {false, QColor("#483D8B")}, // darkstateblue *blue
        {false, QColor("#32CD32")}, // limegreen *green
        {false, QColor("#2E8B57")}, // seagreen *green
        {false, QColor("#2F4F4F")}, // darkslategray *gray
        {false, QColor("#8B008b")}, // darkmagenta *purpure
        {false, QColor("#FF6347")}, // tomato *orange
        {false, QColor("#A0522D")}, // sienna *brown
        {false, QColor("#800000")}, // maroon *red
        {false, QColor("#13B609")}, // *green
        {false, QColor("#0000FF")}  // *blue
    };

public slots:
   void comboBoxChanged(int index);
};


#endif // CUSTOMCOMBOBOX_H
