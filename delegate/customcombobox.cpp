#include "customcombobox.h"

CustomComboBox::CustomComboBox()
{
    setCurrentText("Выбрать цвет");
    for(int i = 0; i < colorRange.size(); ++i)
    {
        addItem(colorRange.at(i).color.name(), colorRange.at(i).color);
        const QModelIndex idx = model()->index(i, 0);
        model()->setData(idx, colorRange.at(i).color, Qt::BackgroundColorRole);
        model()->setData(idx, QColor("black"), Qt::ForegroundRole);
    };
    connect(this, SIGNAL(currentIndexChanged(int)), this, SLOT(comboBoxChanged(int)));

    setStyleSheet("QComboBox { color: Выбрать; background-color: #483D8B;"
                            "selection-color: #ff725c;"
                            "selection-background-color: black;"
                            "font: bold;"
                            "font-size: 12px; }");
}

void CustomComboBox::comboBoxChanged(int index)
{
    QColor forecolor = QColor(itemData(index, Qt::ForegroundRole).value<QColor>());
    QString fgColor = forecolor.name(QColor::HexRgb);

    QColor backcolor = QColor(itemData(index, Qt::BackgroundRole).value<QColor>());
    QString bgColor = backcolor.name(QColor::HexRgb);

    setStyleSheet("QComboBox { color: " + fgColor + "; background-color: " + bgColor + ";"                                                                                            "font-size: 12px; }");
}
