#include "workwindow.h"

int const WorkWindow::EXIT_CODE_REBOOT = -123456789;

WorkWindow::WorkWindow()
{
    setStyleSheet("background-color:lightgray");
    setWindowTitle("Черный ящик");
    createMenuBar();
    createStatusBar();
    createTableItem();
    createGraphic();
    workPaint = new DataWorkPaint(chartview);
}

void WorkWindow::setRangeProgressALL(int rangevalue)
{
    progress->setRange(0,rangevalue);
}

void WorkWindow::stWorkMain(QString datareadwrite)
{
    mutex.lock();
    whatisWork->setText(datareadwrite);
    mutex.unlock();
}

void WorkWindow::eFullscreenOnOff()
{
    if(this->isFullScreen())
        this->setWindowState(Qt::WindowNoState);
    else
        this->setWindowState(Qt::WindowFullScreen);
}

void WorkWindow::createMenuBar()
{
    QMenu *menu = new QMenu;
    menuBar()->setStyleSheet("background-color:#203a40;"
                             "color:#ff726d;"
                             "selection-color: #ff725c;"
                             "selection-background-color: black;"
                             "font: bold;"
                             "font-size: 12px;");
    //split0
    menu = menuBar()->addMenu("Файл");
    menu->setStyleSheet("image: url(:/bbox);"
                        "border-width: 2px;"
                        "border-style: solid;"
                        "border-color: gray");


    //reload bbox
    QAction *menuAction = new QAction;

//    connect(menuAction, &QAction::triggered, this, &WorkWindow::eReboot);
//    QShortcut *rebootShort = new QShortcut(this);
//    rebootShort->setKey(Qt::Key_F2);
//    connect(rebootShort,&QShortcut::activated,this,&WorkWindow::eReboot);


    QAction *data = new QAction(tr("Изменить режим вывода"));
    connect(data, &QAction::triggered, this, &WorkWindow::eChangeMode);
    data->setStatusTip(tr("< F3 > - Смена режима вывода"));
    menu->addAction(data);
    QShortcut *modeChange = new QShortcut(this);
    modeChange->setKey(Qt::Key_F3);
    connect(modeChange,&QShortcut::activated,this,&WorkWindow::eChangeMode);

    data = new QAction("Сохранить данные");
    connect(data, &QAction::triggered, this, &WorkWindow::eSaveData);
    data->setStatusTip(tr("< F5 > - Сохранить график и данные"));
    menu->addAction(data);
    QShortcut *saveData = new QShortcut(this);
    saveData->setKey(Qt::Key_F5);
    connect(saveData,&QShortcut::activated,this,&WorkWindow::eSaveData);

    data = new QAction("Настройки");
    connect(data, &QAction::triggered, this, &WorkWindow::eSettings);
    data->setStatusTip(tr("< F12 > - Открыть окно настроек программы"));
    menu->addAction(data);
    QShortcut *settingsShort = new QShortcut(this);
    settingsShort->setKey(Qt::Key_F12);
    connect(settingsShort,&QShortcut::activated,this,&WorkWindow::eSettings);

    data = new QAction("Выход");
    connect(data, &QAction::triggered, this, &WorkWindow::eExit);
    data->setStatusTip(tr("< CTRL + ESC > - Выход из программы"));
    menu->addAction(data);
    QShortcut *exitShort = new QShortcut(this);
    exitShort->setKey(Qt::Key_Escape);
    connect(exitShort,&QShortcut::activated,this,&WorkWindow::eExit);


    //fullscreen
    QShortcut *sullscreenShort = new QShortcut(this);
    sullscreenShort->setKey(Qt::ALT + Qt::Key_Return);
    connect(sullscreenShort,&QShortcut::activated,this,&WorkWindow::eFullscreenOnOff);


    //Connections
    menu = menuBar()->addMenu("Соединения");// [Текущая - " + CFGD->getCurrentSesion() + "]");
    menu->setStyleSheet("image: url(:/connects);"
                        "border-width: 2px;"
                        "border-style: solid;"
                        "border-color: gray");

    //act00 change session
    QList<QString> sessions = CFGD->getListConnection();
    for(int i=0; i<sessions.size(); i++)
    {
        menuAction = new QAction(sessions.at(i));
        if(CFGD->getCurrentConnect() == menuAction->text())
        {
            menuAction->setStatusTip(tr("Текущее соединение"));
            menuAction->setIcon(QIcon(":/last"));
        }
        menu->addAction(menuAction);
    }
    connect(menu,&QMenu::triggered,this,&WorkWindow::changeSession);
    //act01
}

void WorkWindow::createStatusBar()
{
    //StatusBar
    QStatusBar *statusBar = new QStatusBar();
    whatisWork = new QLabel("DB");
    //statusBar->addPermanentWidget(whatisWork);
    statusBar->addWidget(whatisWork);
    progress = new QProgressBar;
    progress->setValue(0);
    progress->setFixedSize(QSize(100,20));
    statusBar->addPermanentWidget(progress);
    statusBarState = new QLabel("Соединение установлено");
    statusBar->addPermanentWidget(statusBarState);
    statusBar->setStyleSheet("background-color:#203a40;"
                             "color:#ff726d;"
                             "selection-color: #ff725c;"
                             "selection-background-color: black;"
                             "font: bold;"
                             "font-size: 12px;");
    setStatusBar(statusBar);
}

void WorkWindow::createTableItem()
{
    dockViewBase = new QDockWidget("Элементы таблицы БД");

    //titledock
    QWidget *titledock = new QWidget;
    titledock->setMaximumHeight(28);
    titledock->setStyleSheet("background-color:black;"
                             "color:#ff725c;"
                             "font: bold;"
                             "font-size: 10px;");
    QHBoxLayout *titlehlayout = new QHBoxLayout;
    QLabel *title = new QLabel("Дерево БД");
    titlehlayout->addWidget(title);
    titlehlayout->addWidget(showCountSelected);
    titledock->setLayout(titlehlayout);
    dockViewBase->setTitleBarWidget(titledock);
    //titiedock

    //tmp
    QWidget *treeSBase = new QWidget;
    treeSBase->setStyleSheet("background-color:lightgray");
    QVBoxLayout *treeVLayout = new QVBoxLayout;

    onSelected = new QPushButton("Настроить");
    connect(onSelected, &QPushButton::clicked,this,&WorkWindow::showSelectedSetting);
    treeVLayout->addWidget(onSelected);
    dropSelected = new QPushButton("Снять/Очистить элементы");
    connect(dropSelected,&QPushButton::clicked,this,&WorkWindow::setSelectedNULL);
    dropSelected->setVisible(false);
    treeVLayout->addWidget(dropSelected);
    treeSBase->setLayout(treeVLayout);


    searchTreeBase = new QLineEdit;
    QShortcut *cutsearch = new QShortcut(this);
    cutsearch->setKey(Qt::Key_Return);
    connect(cutsearch,&QShortcut::activated,this,&WorkWindow::searchDataTree);
    treeVLayout->addWidget(searchTreeBase);
    treeBase = new DBTableTree();
    connect(treeBase, &QTreeWidget::itemChanged,this,&WorkWindow::stateItemChanged);
    treeVLayout->addWidget(treeBase);
    //TabBuilder *tabBase = new TabBuilder(false);
    //tabBase->newTab(treeSBase,CFGD->getCurrentConnect());
    //dockViewBase->setWidget(tabBase);
    dockViewBase->setWidget(treeSBase);
    //tmp
    addDockWidget(Qt::DockWidgetArea::LeftDockWidgetArea,dockViewBase);
}

void WorkWindow::createGraphic()
{
    QWidget *central = new QWidget;
    QVBoxLayout *vlayout = new QVBoxLayout;
    vlayout->setContentsMargins(2,1,2,1);
    tabCentral = new TabBuilder(false);
    //tmp
    QWidget *wget = new QWidget;
    QVBoxLayout *plotVLayout = new QVBoxLayout;

    QHBoxLayout *plotHLayout = new QHBoxLayout;
    plotHLayout->setAlignment(Qt::AlignLeft);
    plotHLayout->addWidget(title1);
    timeof = new QDateTimeEdit;
    timeof->setDate(QDate(currDT.date().year(),currDT.date().month(),currDT.date().day()));
    timeof->setTime(QTime(currDT.time().hour()-1,currDT.time().minute(),currDT.time().second(),currDT.time().msec()));
    //timeof->setDisplayFormat(dformat);
    timeof->setDisplayFormat(qformat);
    plotHLayout->addWidget(timeof);
    plotHLayout->addWidget(title2);
    timeto = new QDateTimeEdit;
    timeto->setDate(QDate(currDT.date().year(),currDT.date().month(),currDT.date().day()));
    timeto->setTime(QTime(currDT.time().hour(),currDT.time().minute(),currDT.time().second(),currDT.time().msec()));
    //timeto->setDisplayFormat(dformat);
    timeto->setDisplayFormat(qformat);
    plotHLayout->addWidget(timeto);
    //plotVLayout->addLayout(plotHLayout);

    //plotHLayout = new QHBoxLayout;
    //plotHLayout->setAlignment(Qt::AlignLeft);
    repaintPlot = new QPushButton("RT Режим");
    connect(repaintPlot,&QPushButton::clicked,this,&WorkWindow::eChangeMode);
    //connect(repaintPlot,&QPushButton::clicked,this,&WorkWindow::refreshPaint);
    plotHLayout->addWidget(repaintPlot);
    paintPlot = new QPushButton("Отобразить");
    connect(paintPlot,&QPushButton::clicked,this,&WorkWindow::paintDataPlot);
    plotHLayout->addWidget(paintPlot);

    plotVLayout->addLayout(plotHLayout);

    chartview = new GraphicChart;
    connect(chartview, &QCustomPlot::mouseMove, this,&WorkWindow::onMouseMoveGraph);
    //connect(chartview, &GraphicChart::chartdone,this,&WorkWindow::setStatusWork);
    //chartview->setInteraction(QCP::iRangeDrag);
    //chartview->setInteraction(QCP::iRangeZoom);
    //chartview->axisRect()->setRangeDrag(Qt::Horizontal);

    chartview->setBackground(QBrush(QColor("#F0F0F0")));
    chartview->setRangeX(currDT.toMSecsSinceEpoch()-3600000,currDT.toMSecsSinceEpoch());
    plotVLayout->addWidget(chartview);

    wget->setLayout(plotVLayout);

    tabCentral->newTab(wget,CFGD->getCurrentConnect() + " - График");
    //tmp
    vlayout->addWidget(tabCentral);
    central->setLayout(vlayout);
    setCentralWidget(central);
}

QColor WorkWindow::getRangeColor()
{
    QColor returned_color;
    int r;
    int g;
    int b;
    r = rand()%255;
    g = rand()%255;
    b = rand()%255;
    returned_color.setRgb(r,g,b,200);
    return returned_color;
}

void WorkWindow::eSettings()
{
    //add Window Settings
    CFGD->showConfig();
}

void WorkWindow::eHelp()
{
    //add some help
}

//void WorkWindow::eReboot()
//{
//    qDebug() << "REBOTTING";
//    QApplication::exit(WorkWindow::EXIT_CODE_REBOOT);
//}

void WorkWindow::eSaveData()
{
    if(chartview->getListGraph().size()>0)
    {
        //QString dir = QFileDialog::getExistingDirectory(this, tr("Выберите место для сохранения:"), "/home", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
        QString dir = QFileDialog::getSaveFileName(this, tr("Выберите место для сохранения:"), "/home");

        QList<QString> nts;
        if(!dir.isEmpty())
        {
            chartview->savePdf(dir + ".pdf");

            int count = chartview->getListGraph().size();
            QMap<double, QVector<double>> alldata;
            for(int i=0; i<count; i++)
            {
                QCPDataMap* mapget = chartview->getListGraph().at(i)->data();
                nts.append(chartview->getListGraph().at(i)->objectName());
                foreach(QCPData data, mapget->values())
                {
                    double key_add;
                    QVector<double> value_add;
                    value_add.resize(count);
                    key_add = data.key;

                    for(int i=0; i<value_add.size();i++)
                        value_add[i] = -666;

                    if(!alldata.contains(data.key))
                    {

                        value_add[i] = data.value;
                        alldata.insert(key_add,value_add);
                    }
                    else
                    {
                        value_add = alldata.value(key_add);
                        value_add[i] = data.value;
                    }
                    alldata.remove(key_add);
                    alldata.insert(key_add,value_add);
                }
            }

            QVector<double> tempmem;
            tempmem.resize(count);

            int l=0;
            QMap<double, QVector<double>>::const_iterator i = alldata.constBegin();
            while (i != alldata.constEnd())
            {
                QVector<double> tmpdatav = i.value();

                if(l==0)
                    tempmem = tmpdatav;

                for(int c=0; c<tmpdatav.size(); c++)
                {
                    if(QVariant(tmpdatav.at(c)).toString() == QVariant(-666).toString())
                    {
                        if(QVariant(tempmem.at(c)).toString() != QVariant(-666).toString())
                        {
                            tmpdatav[c] = tempmem[c];
                        }
                    }
                    else
                    {
                        if(QVariant(tmpdatav.at(c)).toString() != QVariant(-666).toString())
                            tempmem[c] = tmpdatav.at(c);
                    }
                }

                double key_add;
                key_add = i.key();

                alldata.remove(key_add);
                alldata.insert(key_add,tmpdatav);

                ++i;
                l++;
            }

            if(QFile::exists(dir + ".csv"))
            {
                QFile remfile(dir + ".csv");
                remfile.remove();
            }

            QString named;
            QString desc;
            QList<DataQTO::mDataSelect> data_tmp = *MDE->getMDataSelected();
            for(int cost = 0; cost<nts.size(); cost++)
            {
                QString desctmp;
                for(int post=0; post<data_tmp.size(); post++)
                {
                    DataQTO::mDataSelect data_tmp_list = data_tmp.at(post);
                    QList<DataQTO::DataSelect> data_tmp_list_item = data_tmp_list.items;
                    for(int o=0; o<data_tmp_list_item.size(); o++)
                    {
                        DataQTO::DataSelect data_tmp_list_item_qlist = data_tmp_list_item.at(o);
                        QString dataITEM = data_tmp_list_item_qlist.nameParam;
                        QString namesq = nts.at(cost);
                        auto ddd = namesq.split("[");
                        if(dataITEM == ddd.at(0))
                        {
                            desctmp = data_tmp_list_item_qlist.descParam;
                        }
                    }
                }
                desc = desc + ";" + desctmp;
                desc.replace(","," ");
                named = named + ";" + nts.at(cost);
            }
            qDebug() << named << endl << desc << endl;

            QFile mFile(dir + ".csv");
            mFile.open(QIODevice::Append | QIODevice::Text | QIODevice::ReadWrite);
            QTextStream out(&mFile);

            QTextCodec *codec = QTextCodec::codecForName("UTF-8");
            QByteArray arr = codec->fromUnicode("Время");
            QString time = codec->toUnicode(arr);

            out << time << desc << "\n";
            out << "tm" << named << "\n";


            QMap<double, QVector<double>>::const_iterator b = alldata.constBegin();
            QVector<double> remKeys;
            while (b != alldata.constEnd())
            {
                QString tmpvalue;
                bool removeKey = false;
                for(int c=0; c<b.value().size(); c++)
                {
                    tmpvalue = tmpvalue + ";" + QVariant(b.value().at(c)).toString();
                    if(QVariant(b.value().at(c)).toString().contains("-666"))
                    {
                        removeKey = true;
                        break;
                    }
                }

                if(!removeKey)
                {
                    QDateTime currDT =  QDateTime::fromMSecsSinceEpoch((qint64(b.key())));
                    QString currdatatime = currDT.toString("HH:mm:ss.zzz");
                    currDT =  QDateTime::fromMSecsSinceEpoch((qint64(b.key())));
                    currdatatime = currDT.toString("HH:mm:ss.zzz");
                    out << currdatatime << tmpvalue << ";\n";
                }
                ++b;
            }
            mFile.close();

            QMessageBox *message = new QMessageBox;
            message->setWindowTitle("Сохранение успешно!");
            message->setText("Данные успешно сохранены!\n" + dir);
            message->setIcon(QMessageBox::Information);
            message->addButton("Принять",QMessageBox::YesRole);
            message->show();
        }
    }
    else
    {
        QMessageBox *message = new QMessageBox;
        message->setWindowTitle("Ошибка сохранения!");
        message->setText("Графики отстутствуют! \n"
                         "Выберите переменные и отобразите.");
        message->setIcon(QMessageBox::Warning);
        message->addButton("Принять",QMessageBox::YesRole);
        message->show();
    }
}

void WorkWindow::eExit()
{
    QMessageBox *message = new QMessageBox;
    message->setWindowTitle("Выход");
    message->setText("Вы действительно хотите выйти из программы?");
    message->setIcon(QMessageBox::Information);
    message->addButton("Отменить",QMessageBox::YesRole);
    message->addButton("Принять",QMessageBox::NoRole);
    message->show();
    bool ok = message->exec();
    if(ok)
    {
        this->close();
    }
}

#include <QPair>
#include <QtAlgorithms>

void WorkWindow::eChangeMode()
{
    if(!MDE->getMDataSelected()->isEmpty())
    {
        if(!lifeMode)
        {
            chartview->clearPlot();
            if(!MDE->getMDataSelected()->isEmpty() || MDE->getMDataSelected()->size() != 0)
            {
                for(int i=0; i<MDE->getMDataSelected()->size(); i++)
                {
                    QString tmpquery = query;
                    tmpquery.replace("%nameTable%", MDE->getMDataSelected()->at(i).nameTable);
                    QString tmpname = "tm";
                    for(int k=0; k<MDE->getMDataSelected()->at(i).items.size(); k++)
                    {
                        int ddd = 0;
                        if(MDE->getMDataSelected()->at(i).items.at(k).AlignRLAXISY->checkState() == Qt::CheckState::Checked)
                        {
                            ddd=2;
                        }
                        else
                        {
                            ddd=0;
                        }

                        tmpname = tmpname + ", " + MDE->getMDataSelected()->at(i).items.at(k).nameParam;
                        chartview->newSeries(MDE->getMDataSelected()->at(i).items.at(k).nameParam + "\n[" + MDE->getMDataSelected()->at(i).nameTable + "]",
                                             QVariant(MDE->getMDataSelected()->at(i).items.at(k).colorBox->currentText()).toString(),
                                             ddd,
                                             MDE->getMDataSelected()->at(i).items.at(k).axisSnap->currentIndex(),
                                             MDE->getMDataSelected()->at(i).items.at(k).nameParam + "[" + MDE->getMDataSelected()->at(i).nameTable + "]");//,QVariant(getRangeColor()).toString(),0,0,"");
                    }
                    tmpquery.replace("%nameParam%", tmpname);
                    //qDebug() << lifeMode;
                    whatisWork->setText("RT");
                    RealTimeInstance *RTworker = new RealTimeInstance(tmpquery,"RealTime_" + MDE->getMDataSelected()->at(i).nameTable, CFGD->getConnect(CFGD->getCurrentConnect()));
                    connect(RTworker,&RealTimeInstance::dataout,this,&WorkWindow::outdataRT);
                    connect(this,&WorkWindow::stopwork,RTworker,&RealTimeInstance::getStop);
                    RTworkerLIST.append(RTworker);
                    emit stopwork(false);
                    RTworker->start();
                    //qDebug() << tmpquery;
                }
            }
            statusBarState->setText("Реальное время");
            lifeMode = true;
            repaintPlot->setVisible(false);
            paintPlot->setVisible(false);
            timeof->setVisible(false);
            timeto->setVisible(false);
            title1->setVisible(false);
            title2->setVisible(false);
            progress->setVisible(false);
            dockViewBase->setVisible(false);
            if(currentTop)
                currentTop->setVisible(false);
        }
        else
        {
            lifeMode = false;
            foreach(RealTimeInstance *RTworker, RTworkerLIST)
            {
                emit stopwork(true);
                disconnect(RTworker,&RealTimeInstance::dataout,this,&WorkWindow::outdataRT);
                disconnect(this,&WorkWindow::stopwork,RTworker,&RealTimeInstance::getStop);
                while(RTworker->isRunning() && !RTworker->isFinished()){}
                delete RTworker;
            }
            RTworkerLIST.clear();
            statusBarState->setText("Чтение базы");
            whatisWork->setText("DB");
            repaintPlot->setVisible(true);
            paintPlot->setVisible(true);
            timeof->setVisible(true);
            timeto->setVisible(true);
            title1->setVisible(true);
            title2->setVisible(true);
            progress->setVisible(true);
            dockViewBase->setVisible(true);
            if(currentTop)
                currentTop->setVisible(true);
        }
    }
    else
    {
        QMessageBox *message = new QMessageBox;
        message->setWindowTitle("Отсутствуют параметры для вывода");
        message->setText("Выберите параметры для отображения в режиме RT");
        message->setIcon(QMessageBox::Critical);
        message->addButton("Принять",QMessageBox::YesRole);
        message->show();
    }
}

void WorkWindow::changeSession(QAction *action)
{
    CFGD->LoadConfig(action->text());
    CFGD->DefaultConfig(action->text());
    bool ifchek = CFGD->check_connectdb(action->text());
    if(ifchek)
    {
        delete CFGD;
        CFGD = new ConfigData;
        CFGD->LoadConfig(action->text());
        CFGD->DefaultConfig(action->text());
        CFGD->get_dbc();

        delete MDE;
        MDE = new DataQTO;

        if(monitor != nullptr)
        {
            delete monitor;
            disconnect(monitor,&MonitorWorker::stepStatus,this,&WorkWindow::setStatusWork);
            disconnect(monitor,&MonitorWorker::setRangeProgressQueue,this,&WorkWindow::setRangeProgressALL);
            disconnect(monitor,&MonitorWorker::setValueProgressQueue,this,&WorkWindow::setProgressWork);
            disconnect(monitor,&MonitorWorker::statusWorkMain,this,&WorkWindow::stWorkMain);
        }

        qDebug()<< "i - " << action->text() << ifchek;
        WorkWindow *newWork = new WorkWindow();
        newWork->showFullScreen();
        this->close();
        delete this;
    }
    else if (!ifchek)
    {
        qDebug() << ifchek;
        QMessageBox *message = new QMessageBox;
        message->setWindowTitle("Ошибка соединения!");
        message->setText("<div> "
                         "<font size=\"7\"> (╯ </font>"
                         "<font color=\" red \" size=\"7\"> ° □ ° </font>"
                         "<font size=\"7\"> ）╯ </font> "
                         "<font color=\" black \" size=\"5\"> ︵ ┻━┻ </font> <br/> "
                         "</div>"
                         "<font size=\"4\"> Нет соединения с базой! </font> <br/>" + action->text() + " - недоступен!");
        message->setIcon(QMessageBox::Critical);
        message->addButton("Принять",QMessageBox::YesRole);
        message->show();
    }
}

void WorkWindow::searchDataTree()
{
    QList<QTreeWidgetItem*> all = treeBase->findItems("",Qt::MatchContains | Qt::MatchRecursive);

    foreach(QTreeWidgetItem* item, all)
    {
        item->setHidden(true);
        treeBase->expandItem(item);
    }
    QString tmpstrrsearch = searchTreeBase->text();

    auto splitsearch = tmpstrrsearch.split(";");
    for(int i=0; i<treeBase->columnCount(); i++)
    {
        for(int k=0; k< splitsearch.size(); k++)
        {
            QList<QTreeWidgetItem*> items = treeBase->findItems(splitsearch.at(k),Qt::MatchContains | Qt::MatchRecursive,i);

            foreach(QTreeWidgetItem* item, items)
            {
                item->setHidden(false);
                treeBase->collapseItem(item);

                QTreeWidgetItem* parent = item->parent();
                while (parent)
                {
                    parent->setHidden(false);
                    parent = parent->parent();
                }
            }
        }
    }
}

void WorkWindow::paintDataPlot()
{
    chartview->restore_defAXIS();
    QTreeWidget *selectedMDE = MDE->getSelectedWidget();
    if(selectedMDE && selectedMDE->isVisible())
    {
        QList<DataQTO::mDataSelect> dataSelected = *MDE->getMDataSelected();
        chartview->clearPlot();
        for(int i=0; i<selectedMDE->topLevelItemCount(); i++)
        {
            QString nameTable = selectedMDE->topLevelItem(i)->text(0);
            for(int l=0; l<selectedMDE->topLevelItem(i)->childCount(); l++)
            {
                //search item in selected
                QString color;
                int leftr = 0;
                int axein = 0;
                QString nametable;
                QString nameitem;
                for(int k=0; k<dataSelected.size(); k++)
                {
                    if(dataSelected.at(k).nameTable == nameTable)
                    {
                        for(int z=0; z<dataSelected.at(k).items.size(); z++)
                        {
                            if(dataSelected.at(k).items.at(z).nameParam == selectedMDE->topLevelItem(i)->child(l)->text(0))
                            {
                                color = dataSelected.at(k).items.at(z).colorBox->itemText(dataSelected.at(k).items.at(z).colorBox->currentIndex());
                                leftr = dataSelected.at(k).items.at(z).AlignRLAXISY->checkState();
                                axein = QVariant(dataSelected.at(k).items.at(z).axisSnap->itemText(dataSelected.at(k).items.at(z).axisSnap->currentIndex())).toInt();
                                nameitem = dataSelected.at(k).items.at(z).nameParam;
                            }
                        }
                        nametable = dataSelected.at(k).nameTable;
                    }
                }
                //search item in selected
                chartview->newSeries(selectedMDE->topLevelItem(i)->child(l)->text(0) + "\n[" + nameTable + "]",color,leftr,axein,nameitem + "[" + nametable + "]");
            }
        }
        chartview->setInteraction(QCP::iRangeDrag);
        chartview->setInteraction(QCP::iRangeZoom);
        chartview->axisRect()->setRangeZoom(Qt::Horizontal);
    }
    else
    {
        QList<DataQTO::mDataSelect> dataSelected = *MDE->getMDataSelected();
        chartview->clearPlot();
        if(!dataSelected.isEmpty())
        {
            for(int i=0; i<dataSelected.size(); i++)
            {
                for(int k=0; k< dataSelected.at(i).items.size(); k++)
                {
                    chartview->newSeries(dataSelected.at(i).items.at(k).nameParam + "\n[" + dataSelected.at(i).nameTable + "]",QVariant(getRangeColor()).toString(),0,0,"");
                }
            }
        }
        chartview->setInteraction(QCP::iRangeDrag);
        chartview->setInteraction(QCP::iRangeZoom);
        chartview->axisRect()->setRangeZoom(Qt::Horizontal);
    }
    if(monitor)
    {
        MDE->nullTotalSize();
        delete monitor;
        disconnect(monitor,&MonitorWorker::stepStatus,this,&WorkWindow::setStatusWork);
        disconnect(monitor,&MonitorWorker::setRangeProgressQueue,this,&WorkWindow::setRangeProgressALL);
        disconnect(monitor,&MonitorWorker::setValueProgressQueue,this,&WorkWindow::setProgressWork);
        disconnect(monitor,&MonitorWorker::statusWorkMain,this,&WorkWindow::stWorkMain);
    }
    monitor = new MonitorWorker(timeof->text(),timeto->text(),workPaint);
    connect(monitor,&MonitorWorker::stepStatus,this,&WorkWindow::setStatusWork);
    connect(monitor,&MonitorWorker::setRangeProgressQueue,this,&WorkWindow::setRangeProgressALL);
    connect(monitor,&MonitorWorker::setValueProgressQueue,this,&WorkWindow::setProgressWork);
    connect(monitor,&MonitorWorker::statusWorkMain,this,&WorkWindow::stWorkMain);
    monitor->startMonitor();

}

void WorkWindow::refreshPaint()
{
    QMessageBox *message = new QMessageBox;
    message->setWindowTitle("Текущие значения!");
    QString textMdata;
//    QList<DataQTO::mDataSelect> data = *MDE->getMDataSelected();
//    for(int i=0; i<data.size(); i++)
//    {
//        textMdata.append("[" + data.at(i).nameTable + "]\n");
//        for(int k=0; k<data.at(i).items.size(); k++)
//        {
//            //textMdata.append("= " + data.at(i).items.at(k).nameParam /*+ ":" + data.at(i).items.at(k).descParam*/
//            //                + ":(sIndex) " + QVariant(data.at(i).items.at(k).axisSnap->currentIndex()).toString() + ":(cIndex) " + QVariant(data.at(i).items.at(k).colorBox->currentIndex()).toString() + "\n");
//        }
//    }
    textMdata = QVariant(chartview->axisRectCount()).toString();
    message->setText(textMdata);
    message->setIcon(QMessageBox::Critical);
    message->addButton("Принять",QMessageBox::YesRole);
    message->show();

}

void WorkWindow::setStatusWork(QString status)
{
    mutex.lock();
    statusBarState->setText(status);
    mutex.unlock();
}

void WorkWindow::setProgressWork(int gvalue)
{
    mutex.lock();
    progress->setValue(progress->maximum() - gvalue);
    if(progress->maximum() == progress->value())
    {
        repaintPlot->show();
        paintPlot->show();
    }
    mutex.unlock();
}

void WorkWindow::onMouseMoveGraph(QMouseEvent *evt)
{
    double x = chartview->xAxis->pixelToCoord(evt->pos().x());
    QString cuurent_time = QDateTime::fromMSecsSinceEpoch(qint64(x)).toString("HH:mm:ss.zzz\nyyyy-MM-dd");
    double y = chartview->yAxis->pixelToCoord(evt->pos().y());
    setToolTip(QString("[%2]\n%1").arg(cuurent_time).arg(y));
}

void WorkWindow::showSelectedSetting()
{
    disconnect(onSelected, &QPushButton::clicked,this,&WorkWindow::showSelectedSetting);
    connect(onSelected, &QPushButton::clicked,this,&WorkWindow::cancelSettingsShow);
    onSelected->setText("Отмена");
    if(MDE->getMDataSelected()->size()>0)
        dropSelected->setVisible(false);
    currentTop = MDE->showSelected();
    addDockWidget(Qt::DockWidgetArea::TopDockWidgetArea,currentTop);
    currentTop->show();
    searchTreeBase->setDisabled(true);
    treeBase->setDisabled(true);

//    QMessageBox *message = new QMessageBox;
//    message->setWindowTitle("Текущие значения!");
//    QString textMdata;
//    QList<DataQTO::mDataSelect> data = *MDE->getMDataSelected();
//    for(int i=0; i<data.size(); i++)
//    {
//        textMdata.append("[" + data.at(i).nameTable + "]\n");
//        for(int k=0; k<data.at(i).items.size(); k++)
//        {
//            textMdata.append("= " + data.at(i).items.at(k).nameParam /*+ ":" + data.at(i).items.at(k).descParam*/
//                             + ":(sIndex) " + QVariant(data.at(i).items.at(k).axisSnap->currentIndex()).toString() + ":(cIndex) " + QVariant(data.at(i).items.at(k).colorBox->currentIndex()).toString() + "\n");
//        }
//    }

//    message->setText(textMdata);r
//    message->setIcon(QMessageBox::Critical);
//    message->addButton("Принять",QMessageBox::YesRole);
    //    message->show();
}

void WorkWindow::cancelSettingsShow()
{
    connect(onSelected, &QPushButton::clicked,this,&WorkWindow::showSelectedSetting);
    disconnect(onSelected, &QPushButton::clicked,this,&WorkWindow::cancelSettingsShow);
    onSelected->setText("Настроить");
    if(MDE->getMDataSelected()->size()>0)
        dropSelected->setVisible(true);
    removeDockWidget(currentTop);
    currentTop = MDE->showSelected();
    searchTreeBase->setDisabled(false);
    treeBase->setDisabled(false);
}

void WorkWindow::stateItemChanged(QTreeWidgetItem *item, int column)
{
    QTreeWidgetItem *tmpItem = static_cast<QTreeWidgetItem*>(item);
    Q_UNUSED(column);
    if(!tmpItem->text(0).contains("[")&&!tmpItem->text(0).contains("]"))
    {
        if(tmpItem->checkState(0) == Qt::CheckState::Unchecked)
        {
            colselected--;
            MDE->removeSelected(tmpItem->parent()->text(0),tmpItem->text(0));
            if(QVariant(colselected).toString() == "0")
                dropSelected->setVisible(false);

        }
        else
        {
            if(colselected != 11)
            {
                colselected++;
                MDE->addNewSelected(tmpItem->parent()->text(0).split("[")[0],tmpItem->text(0),tmpItem->text(1));
                if(colselected == 1)
                    dropSelected->setVisible(true);
            }
        }
    }
    else
    {
        if(tmpItem->checkState(0) == Qt::CheckState::Unchecked)
        {
        for(int i=0; i<tmpItem->childCount(); i++)
        {
            if(!tmpItem->child(i)->isHidden())
            {
                tmpItem->child(i)->setCheckState(0,Qt::CheckState::Unchecked);
                MDE->removeSelected(tmpItem->parent()->text(0),tmpItem->child(i)->text(0));

            }
        }
        }
        else
        {
            if(tmpItem->checkState(0) == Qt::CheckState::Checked)
            {
            for(int i=0; i<tmpItem->childCount(); i++)
            {
                if(!tmpItem->child(i)->isHidden())
                {
                    tmpItem->child(i)->setCheckState(0,Qt::CheckState::Checked);
                    MDE->addNewSelected(tmpItem->text(0).split("[")[0],tmpItem->child(i)->text(0),tmpItem->child(i)->text(1));
                }
            }
            }
        }
    }
    if(colselected == 11)
    {
        tmpItem->setCheckState(0,Qt::CheckState::Unchecked);
    }
    if(colselected >0)
    {
        QString tmpstr = QVariant(colselected).toString();
        if(colselected == 10)
        {
            tmpstr = tmpstr + " [MAX]";
        }
        showCountSelected->setText("Выбрано " + tmpstr);
    }
    else
    {
        showCountSelected->setText("");
    }
}

void WorkWindow::outdataRT(QString nameTable, QVector<double> keyData, QList<QString> nameParams, QVector<QVector<double> > ValueData)
{
    workPaint->enqueuePaint(nameTable, keyData, nameParams, ValueData);
}

void WorkWindow::setSelectedNULL()
{
    treeBase->dropALLSelected();
    MDE->clearDataSelected();
    dropSelected->setVisible(false);
}
