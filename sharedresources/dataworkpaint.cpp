#include "dataworkpaint.h"

DataWorkPaint::DataWorkPaint(GraphicChart *chartData)
{
    timetout = new QTimer;
    timetout->setInterval(100);
    connect(timetout, &QTimer::timeout, this, &DataWorkPaint::deueue);
    timetout->start();
    this->chartData = chartData;
}

void DataWorkPaint::enqueuePaint(QString nameTable, QVector<double> keyData, QList<QString> nameParams, QVector<QVector<double>> ValueData)
{
    mutex.lock();
    DataWorkPaint::dataToPaint dataadd;
    dataadd.nameTable = nameTable;
    dataadd.keyData = keyData;
    dataadd.nameParams = nameParams;
    dataadd.ValueData = ValueData;
    outdata.enqueue(dataadd);
    if(!timetout->isActive())
    {
        timetout->start();
    }
    mutex.unlock();
}

void DataWorkPaint::deueue()
{
    mutex.lock();
    while(!outdata.isEmpty())
    {
        DataWorkPaint::dataToPaint dataadd = outdata.dequeue();
        for(int i=0; i<dataadd.nameParams.size(); i++)
        {
            if(dataadd.keyData.size()>0 && dataadd.ValueData.at(i).size()>0)
            chartData->addNewData(dataadd.nameParams.at(i) + "[" + dataadd.nameTable + "]", dataadd.keyData, dataadd.ValueData.at(i));
        }
    }
    if(outdata.isEmpty())
    {
        timetout->stop();
    }
    mutex.unlock();
}
