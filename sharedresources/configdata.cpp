#include "configdata.h"

ConfigData::ConfigData()
{
    listConnect = getListConnect();
    if(listConnect.isEmpty())
        DefaultConfig("default");
}

void ConfigData::LoadConfig(QString name_ini)
{
    this->settings = new QSettings(name_ini + ".ini", QSettings::IniFormat);
    this->settings->setIniCodec(codec);
    this->settings->beginGroup(name_ini);
    const QStringList childKeys = this->settings->childKeys();
    foreach (const QString &childKey, childKeys)
        this->settings->value(childKey).toString();

    this->currentNameSession = name_ini;
    this->typebd = settings->value("TYPE").toString();
    this->namedb = settings->value("BASE").toString();
    this->hostname = settings->value("IP").toString();
    this->port = settings->value("PORT").toInt();
    this->user = settings->value("USER").toString();
    this->password = settings->value("PASSWORD").toString();
    this->workerSize = settings->value("WORKER").toInt();
    this->delmitter = settings->value("TIMEDEL").toInt();
}

QSqlDatabase *ConfigData::get_dbc()
{
    LoadConfig(currentNameSession);
    if(default_db.hostName().isEmpty())
    {
        default_db = QSqlDatabase::addDatabase(typebd,"tableInstance");
    }
    default_db.setHostName(hostname);
    default_db.setPort(port);
    default_db.setDatabaseName(namedb);
    default_db.setUserName(user);
    default_db.setPassword(password);
    //qDebug() << default_db.connectionName();

    bool ok = this->default_db.open();
    if(ok)
    {
        return &default_db;
    }
    else
    {
        return &default_db;
    }
}

void ConfigData::showConfig()
{
    if(!settingsw)
    {
        createWindowSettings();
    }
    ndb->setText(namedb);
    ip->setText(hostname);
    portw->setText(QVariant(this->port).toString());
    userw->setText(this->user);
    passwordw->setText(this->password);
    workers->setText(QVariant(workerSize).toString());
    delmitterw->setText(QVariant(this->delmitter).toString());

//    settingsw->setStyleSheet("background-color:lightgray");
//    settingsw->setWindowTitle("Настройки");

//    vlay->addWidget(new QLabel("<div>"
//                               "<font color=\" darkred \" size=\"4\"> Параметры соединения: </font>"
//                               "</div>"));

//    hlay->addWidget(new QLabel("Имя Базы:"));
//    ndb->setStyleSheet("background-color:white");
//    ndb->setReadOnly(true);
//    ndb->setText(namedb);
//    hlay->addWidget(ndb);
//    vlay->addLayout(hlay);

//    hlay = new QHBoxLayout;
//    hlay->addWidget(new QLabel("IP - Адрес:"));
//    ip->setStyleSheet("background-color:white");
//    ip->setReadOnly(true);
//    ip->setText(hostname);
//    hlay->addWidget(ip);
//    hlay->addWidget(new QLabel("Порт:"));
//    portw->setStyleSheet("background-color:white");
//    portw->setReadOnly(true);
//    portw->setText(QVariant(this->port).toString());
//    hlay->addWidget(portw);
//    vlay->addLayout(hlay);

//    hlay = new QHBoxLayout;
//    hlay->addWidget(new QLabel("Пользователь:"));
//    userw->setStyleSheet("background-color:white");
//    userw->setReadOnly(true);
//    userw->setText(this->user);
//    hlay->addWidget(userw);
//    hlay->addWidget(new QLabel("Пароль:"));
//    passwordw->setStyleSheet("background-color:white");
//    passwordw->setReadOnly(true);
//    passwordw->setText(this->password);
//    hlay->addWidget(passwordw);
//    vlay->addLayout(hlay);


//    vlay->addWidget(new QLabel("<div>"
//                               "<font color=\" darkred \" size=\"4\"> Параметры программы: </font>"
//                               "</div>"));

//    hlay = new QHBoxLayout;
//    hlay->addWidget(new QLabel("Количество воркеров:"));
//    workers->setStyleSheet("background-color:white");
//    workers->setText(QVariant(workerSize).toString());
//    workers->setInputMask("99");
//    hlay->addWidget(workers);
//    vlay->addLayout(hlay);
//    hlay = new QHBoxLayout;
//    hlay->addWidget(new QLabel("Делитель времени(мин):"));

//    delmitterw->setStyleSheet("background-color:white");
//    delmitterw->setText(QVariant(this->delmitter).toString());
//    delmitterw->setInputMask("99");
//    hlay->addWidget(delmitterw);
//    vlay->addLayout(hlay);

//    vlay->addWidget(new QLabel("<div>"
//                               "<font color=\" darkred \" size=\"4\"> Параметры графика: </font>"
//                               "</div>"));

//    hlay = new QHBoxLayout;
//    hlay->addWidget(new QLabel("Режим выбора:"));
//    modout->setStyleSheet("background-color:white;"
//                          "color:black;"
//                          "selection-color: white;"
//                          "selection-background-color: gray;");
//    modout->addItem("Выделение цветом");
//    modout->addItem("Выделение по точкам");
//    hlay->addWidget(modout);
//    vlay->addLayout(hlay);

//    hlay = new QHBoxLayout;
//    hlay->addWidget(ok);
//    connect(ok,&QPushButton::clicked,this,&ConfigData::okAction);
//    hlay->addWidget(neok);
//    connect(neok,&QPushButton::clicked,this,&ConfigData::neokAction);
//    vlay->addLayout(hlay);


//    settingsw->setLayout(vlay);

    settingsw->show();
}

int ConfigData::getWorkerCount()
{
    return this->workerSize;
}

int ConfigData::getTimeSplit()
{
    return  this->delmitter;
}

void ConfigData::createWindowSettings()
{
    settingsw = new QWidget;
    settingsw->setStyleSheet("background-color:lightgray");
    settingsw->setWindowTitle("Настройки");

    vlay->addWidget(new QLabel("<div>"
                               "<font color=\" darkred \" size=\"4\"> Параметры соединения: </font>"
                               "</div>"));

    hlay->addWidget(new QLabel("Имя Базы:"));
    ndb->setStyleSheet("background-color:white");
    ndb->setReadOnly(true);
    hlay->addWidget(ndb);
    vlay->addLayout(hlay);

    hlay = new QHBoxLayout;
    hlay->addWidget(new QLabel("IP - Адрес:"));
    ip->setStyleSheet("background-color:white");
    ip->setReadOnly(true);
    hlay->addWidget(ip);
    hlay->addWidget(new QLabel("Порт:"));
    portw->setStyleSheet("background-color:white");
    portw->setReadOnly(true);
    hlay->addWidget(portw);
    vlay->addLayout(hlay);

    hlay = new QHBoxLayout;
    hlay->addWidget(new QLabel("Пользователь:"));
    userw->setStyleSheet("background-color:white");
    userw->setReadOnly(true);
    hlay->addWidget(userw);
    hlay->addWidget(new QLabel("Пароль:"));
    passwordw->setStyleSheet("background-color:white");
    passwordw->setReadOnly(true);
    hlay->addWidget(passwordw);
    vlay->addLayout(hlay);


    vlay->addWidget(new QLabel("<div>"
                               "<font color=\" darkred \" size=\"4\"> Параметры программы: </font>"
                               "</div>"));

    hlay = new QHBoxLayout;
    hlay->addWidget(new QLabel("Количество воркеров:"));
    workers->setStyleSheet("background-color:white");
    //workers->setInputMask("99");
    hlay->addWidget(workers);
    vlay->addLayout(hlay);
    hlay = new QHBoxLayout;
    hlay->addWidget(new QLabel("Делитель времени(мин):"));

    delmitterw->setStyleSheet("background-color:white");
    //delmitterw->setInputMask("99");
    hlay->addWidget(delmitterw);
    vlay->addLayout(hlay);

//    vlay->addWidget(new QLabel("<div>"
//                               "<font color=\" darkred \" size=\"4\"> Параметры графика: </font>"
//                               "</div>"));

//    hlay = new QHBoxLayout;
//    hlay->addWidget(new QLabel("Режим выбора:"));
//    modout->setStyleSheet("background-color:white;"
//                          "color:black;"
//                          "selection-color: white;"
//                          "selection-background-color: gray;");
//    modout->addItem("Выделение цветом");
//    modout->addItem("Выделение по точкам");
//    hlay->addWidget(modout);
//    vlay->addLayout(hlay);

    hlay = new QHBoxLayout;
    hlay->addWidget(ok);
    connect(ok,&QPushButton::clicked,this,&ConfigData::okAction);
    hlay->addWidget(neok);
    connect(neok,&QPushButton::clicked,this,&ConfigData::neokAction);
    vlay->addLayout(hlay);


    settingsw->setLayout(vlay);

}

void ConfigData::DefaultConfig(QString name_ini)
{
    if(!QFile::exists(name_ini + ".ini"))
    {
        this->currentNameSession = name_ini;
        this->settings = new QSettings(name_ini + ".ini", QSettings::IniFormat);
        this->settings->setIniCodec(codec);
        this->settings->beginGroup(name_ini);
        this->settings->setValue("TYPE",typebd);
        this->settings->setValue("IP",hostname);
        this->settings->setValue("PORT",port);
        this->settings->setValue("BASE",namedb);
        this->settings->setValue("USER",user);
        this->settings->setValue("PASSWORD",password);
        this->settings->setValue("WORKER",workerSize);
        this->settings->setValue("TIMEDEL",delmitter);
        this->settings->endGroup();
        this->settings->sync();
        LoadConfig(name_ini);
    }
    else
    {
        LoadConfig(name_ini);
    }
}

QList<QString> ConfigData::getListConnection()
{
    getListConnect();
    return this->listConnect;
}

void ConfigData::deleteConnect(QString nameConnect)
{
    for(int i=0; i<listConnect.size(); i++)
        if(listConnect.at(i) == nameConnect)
        {
            listConnect.removeAt(i);
            if(QFile::exists(nameConnect + ".ini"))
            {
                QFile remfile(nameConnect + ".ini");
                remfile.remove();
            }
        }
}

QSettings *ConfigData::getConnect(QString nameConnect)
{
    this->currentNameSession = nameConnect;
    LoadConfig(nameConnect);
    return this->settings;
}

QString ConfigData::getCurrentConnect()
{
    return this->currentNameSession;
}

void ConfigData::saveSession()
{
    this->settings->sync();
}

void ConfigData::setNewConfig(QString nameconnect, QString typebd, QString ip, int port, QString base, QString user, QString password)
{
    bool check = false;
    for(int i=0; i<listConnect.size(); i++)
        if(listConnect.at(i) == nameconnect)
        {
            check = true;
            qDebug() << "SESSION EXIST " << nameconnect;
            this->currentNameSession = nameconnect;
            this->settings = new QSettings(nameconnect + ".ini", QSettings::IniFormat);
            this->settings->setIniCodec(codec);
            this->settings->beginGroup(nameconnect);
            this->settings->setValue("TYPE",typebd);
            this->settings->setValue("IP",ip);
            this->settings->setValue("PORT",port);
            this->settings->setValue("BASE",base);
            this->settings->setValue("USER",user);
            this->settings->setValue("PASSWORD",password);
            this->settings->setValue("WORKER",workerSize);
            this->settings->setValue("TIMEDEL",delmitter);
            this->settings->endGroup();
            this->settings->sync();
            qDebug() << "SESSION CHANGED";
        }
    if(!check)
    {
        this->currentNameSession = nameconnect;
        this->settings = new QSettings(nameconnect + ".ini", QSettings::IniFormat);
        this->settings->setIniCodec(codec);
        this->settings->beginGroup(nameconnect);
        this->settings->setValue("TYPE",typebd);
        this->settings->setValue("IP",ip);
        this->settings->setValue("PORT",port);
        this->settings->setValue("BASE",base);
        this->settings->setValue("USER",user);
        this->settings->setValue("PASSWORD",password);
        this->settings->setValue("WORKER",workerSize);
        this->settings->setValue("TIMEDEL",delmitter);
        this->settings->endGroup();
        this->settings->sync();
    }
}

bool ConfigData::check_connectdb(QString namedbconnect)
{
    lastConnect = currentNameSession;
    LoadConfig(namedbconnect);
    QSqlDatabase base = QSqlDatabase::addDatabase(typebd,namedbconnect);
    base.setDatabaseName(namedb);
    base.setHostName(hostname);
    base.setPort(port);
    base.setUserName(user);
    base.setPassword(password);
    bool ok = base.open();
    if(ok)
    {
        qDebug() << "База:" << namedbconnect << "isConnected" << endl;
        lastConnect.clear();
    }
    else
    {
        LoadConfig(lastConnect);
        qDebug() << "База:" << namedbconnect << "noConnected" << endl;
    }
    return ok;
}

QList<QString> ConfigData::getListConnect()
{
    QDir dir;
    QStringList nameFilter;
    nameFilter << "*.ini";
    QFileInfoList list = dir.entryInfoList(nameFilter, QDir::Files);

    QFileInfo fileinfo;
    foreach (fileinfo, list)
    {
        bool ok = false;
        QString current = fileinfo.fileName().split(".")[0];
        if(!listConnect.isEmpty())
        {
            for(int i=0;i<listConnect.size(); i++)
            {
                if(listConnect.at(i).contains(current))
                {
                    ok = true;
                }
            }
            if(!ok)
                listConnect.append(current);
        }
        else
        {
            listConnect.append(current);
        }
    }
    return listConnect;
}

void ConfigData::neokAction()
{
    settingsw->close();
}

void ConfigData::okAction()
{
    this->settings = new QSettings(currentNameSession + ".ini", QSettings::IniFormat);
    this->settings->setIniCodec(codec);
    this->settings->beginGroup(currentNameSession);
    this->settings->setValue("TYPE",typebd);
    this->settings->setValue("IP",ip->text());
    this->hostname = ip->text();
    this->settings->setValue("PORT",portw->text());
    this->port = QVariant(portw->text()).toInt();
    this->settings->setValue("BASE",ndb->text());
    this->namedb = ndb->text();
    this->user = userw->text();
    this->settings->setValue("USER",userw->text());
    this->password = passwordw->text();
    this->settings->setValue("PASSWORD",passwordw->text());
    this->workerSize = QVariant(workers->text()).toInt();
    this->settings->setValue("WORKER",QVariant(workers->text()).toInt());
    this->delmitter = QVariant(delmitterw->text()).toInt();
    this->settings->setValue("TIMEDEL",QVariant(delmitterw->text()).toInt());
    this->settings->endGroup();
    this->settings->sync();
    settingsw->close();
}
