#ifndef CONFIGDATA_H
#define CONFIGDATA_H

#include <QSettings>
#include <QDebug>
#include <QDir>
#include <QTextCodec>
#include <QMutex>
#include <QSqlDatabase>
#include <QSqlQuery>

#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QLabel>
#include <QComboBox>
#include <QPushButton>
#include <QObject>

class ConfigData : public QObject
{
    Q_OBJECT
public:
    explicit ConfigData();
    void DefaultConfig(QString name_ini);
    QList<QString> getListConnection();
    void deleteConnect(QString nameConnect);
    QSettings *getConnect(QString nameConnect);
    QString getCurrentConnect();
    void saveSession();
    void setNewConfig(QString nameconnect, QString typebd, QString ip, int port, QString base, QString user, QString password);
    bool check_connectdb(QString namedbconnect);
    void LoadConfig(QString name_ini);
    QSqlDatabase *get_dbc();
    void showConfig();
    int getWorkerCount();
    int getTimeSplit();
//signals:
//    void changeConnectConfig();

private:
    QSqlDatabase default_db;
    QList<QString> listConnect;
    QSettings *settings;
    QTextCodec *codec = QTextCodec::codecForName("Windows-1251");

    QString lastConnect;
    QString currentNameSession = "combo";
    QString typebd = "QPSQL";
    QString hostname = "192.168.10.30";
    int port = 5432;
    QString namedb = "combo";
    QString user = "combo";
    QString password = "162747";

    int workerSize = 5;
    int delmitter = 15;

    //form
    QHBoxLayout *hlay = new QHBoxLayout;
    QVBoxLayout *vlay = new QVBoxLayout;

    QLineEdit *ndb = new QLineEdit;
    QLineEdit *ip = new QLineEdit;
    QLineEdit *portw = new QLineEdit;
    QLineEdit *workers = new QLineEdit;
    QLineEdit *delmitterw = new QLineEdit;
    QComboBox *modout = new QComboBox;
    QLineEdit *userw = new QLineEdit;
    QLineEdit *passwordw = new QLineEdit;

    QWidget *settingsw = nullptr;
    QPushButton *ok = new QPushButton("Принять");
    QPushButton *neok = new QPushButton("Отмена");

    void createWindowSettings();
    QList<QString> getListConnect();

private slots:
    void neokAction();
    void okAction();
};

#endif // CONFIGDATA_H
