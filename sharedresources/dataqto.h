﻿#ifndef DATAQTO_H
#define DATAQTO_H

#include <QString>
#include <QVector>
#include <QList>
#include <QMutex>
#include <QDockWidget>
#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QTreeWidget>
#include <QLabel>
#include <QComboBox>
#include <QColormap>
#include <QItemDelegate>
#include <QColor>
#include <QPushButton>
#include <QCheckBox>

#include <QSettings>

#include <QObject>
#include <QDebug>
#include <QTextCodec>
#include <QFileDialog>

#include "delegate/customcombobox.h"

class DataQTO : public QObject
{
    Q_OBJECT
public:
    DataQTO();
    //for(TreeBaseWidget)
    struct dataTable {QString name; QString description;};
    //for(TreeBaseWidget) full table&items
    struct table {QString name; QList<QString> valueTM; struct item {QString name; QVector<double> value;}; QList<item> items;};
    //for(selected_items) from(TreeBaseWidget)
    struct DataSelect {QString nameParam; QString descParam; /*int colorIndex*/ CustomComboBox *colorBox; /*int axisSnap;*/QComboBox *axisSnap; QCheckBox *AlignRLAXISY;};
    struct mDataSelect {QList<DataQTO::DataSelect> items; QString nameTable;};

    QList<DataQTO::mDataSelect> *getMDataSelected();

    //record *.Size();
    void addToTotalSize(int size);
    void nullTotalSize();
    int getTotalSize();
    QMutex mutex;

    QSettings *saveData;
    QTextCodec *codec = QTextCodec::codecForName("Windows-1251");

    void addNewSelected(QString Table, QString Name, QString Description);
    void removeSelected(QString Table, QString Name);
    QDockWidget *showSelected();

    void clearDataSelected();

    QTreeWidget *getSelectedWidget();

private:
    QDockWidget *selectedItemsDock = new QDockWidget("Настройки выбранных элементов");
    QList<DataQTO::mDataSelect> mDataSelected;
    int totalDataSize = 0;
    QWidget *cent = nullptr;
    QVBoxLayout *vlay = nullptr;
    QWidget *titledock = nullptr;
    QTreeWidget *treeSelected = nullptr;
    QPushButton *saveSettings = nullptr;
    QPushButton *loadSettings = nullptr;

    void setUPDock();

private:
     void saveSettingsSelected();
     void loadSettingsSelected();
     void changedSelected();

private slots:
     void changetSetSelected(QTreeWidgetItem *item, int column);
     //void SAVEitems();
     //void LOADitems();

};

#endif // DATAQTO_H
