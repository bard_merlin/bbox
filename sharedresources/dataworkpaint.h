#ifndef DATAWORKPAINT_H
#define DATAWORKPAINT_H

#include <QDebug>
#include <QQueue>
#include <QString>
#include <QTimer>
#include <QObject>
#include <QMutex>
#include "graphicdata/graphicchart.h"

class DataWorkPaint : public QObject
{
    Q_OBJECT
public:
    struct mDataTQuery
    {
        QList<QString> nameParams;
        QString nameTable;
    };

    struct dataToPaint
    {
        QString nameTable;
        QVector<double> keyData;
        QList<QString> nameParams;
        QVector<QVector<double>> ValueData;
    };

    DataWorkPaint(GraphicChart *chartData);
    QMutex mutex;

private:
    QQueue<DataWorkPaint::dataToPaint> outdata;
    GraphicChart *chartData;
    QTimer *timetout;

public slots:
    void enqueuePaint(QString nameTable, QVector<double> keyData, QList<QString> nameParams, QVector<QVector<double>> ValueData);

private slots:
    void deueue();
};


#endif // DATAWORKPAINT_H
