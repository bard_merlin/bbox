#include "dataqto.h"

DataQTO::DataQTO()
{
    titledock = new QWidget;
    titledock->setMaximumHeight(28);
    titledock->setStyleSheet("background-color:black;"
                             "color:#ff725c;"
                             "font: bold;"
                             "font-size: 10px;");
    QHBoxLayout *titlehlayout = new QHBoxLayout;
    QLabel *title = new QLabel("Настройки выбранных параметров");
    titlehlayout->addWidget(title);
    titledock->setLayout(titlehlayout);
    selectedItemsDock->setTitleBarWidget(titledock);
}

QList<DataQTO::mDataSelect> *DataQTO::getMDataSelected()
{
    return &mDataSelected;
}

void DataQTO::addToTotalSize(int size)
{
    mutex.lock();
    this->totalDataSize = totalDataSize + size;
    mutex.unlock();
}

void DataQTO::nullTotalSize()
{
    mutex.lock();
    this->totalDataSize = 0;
    mutex.unlock();
}

int DataQTO::getTotalSize()
{
    return this->totalDataSize;
}

QDockWidget *DataQTO::showSelected()
{
    if (saveSettings)
        disconnect(saveSettings, &QPushButton::clicked, this, &DataQTO::saveSettingsSelected);
    if(treeSelected)
        disconnect(treeSelected, &QTreeWidget::itemChanged, this, &DataQTO::changetSetSelected);
    setUPDock();

    QList<QString> cbTexts;
    int t=0;
    for(int i=0; i<mDataSelected.size(); i++)
    {
        for(int b=0; b<mDataSelected.at(i).items.size(); b++)
        {
            cbTexts.append(QVariant(t).toString());
            t++;
        }
    }

    for(int i=0; i<mDataSelected.size(); i++)
    {
        QTreeWidgetItem *mainitem = new QTreeWidgetItem;
        mainitem->setText(0,mDataSelected.at(i).nameTable);
        treeSelected->addTopLevelItem(mainitem);
        for(int r=0; r<mDataSelected.at(i).items.size(); r++)
        {
            QTreeWidgetItem *item = new QTreeWidgetItem;
            item->setText(0,mDataSelected.at(i).items.at(r).nameParam);
            item->setText(1,mDataSelected.at(i).items.at(r).descParam);
            mainitem->addChild(item);
            if(mDataSelected.at(i).items.at(r).colorBox)
            {
                QCheckBox *checkRL = mDataSelected.at(i).items.at(r).AlignRLAXISY;
                checkRL->setMinimumSize(20,10);
                treeSelected->setItemWidget(item,4,checkRL);
                CustomComboBox *boxcolor = mDataSelected.at(i).items.at(r).colorBox;
                treeSelected->setItemWidget(item,3,boxcolor);
                QComboBox *box = mDataSelected.at(i).items.at(r).axisSnap;
                if(box->itemText(0).isEmpty())
                {
                    box->clear();
                    box->addItems(cbTexts);
                }
                treeSelected->setItemWidget(item,2,box);
            }
        }
    }
    treeSelected->expandAll();
    treeSelected->resizeColumnToContents(0);
    treeSelected->resizeColumnToContents(1);
    return selectedItemsDock;
}

void DataQTO::clearDataSelected()
{
    mDataSelected.clear();
}

QTreeWidget *DataQTO::getSelectedWidget()
{
    return this->treeSelected;
}

void DataQTO::setUPDock()
{
    cent = new QWidget;
    vlay = new QVBoxLayout;
    treeSelected = new QTreeWidget;

    treeSelected->setStyleSheet("QTreeView::item {"
                      "padding: 3px;"
                      "}");
    QHBoxLayout *hlaytmp = new QHBoxLayout;

    loadSettings = new QPushButton("Загрузить");
    hlaytmp->addWidget(loadSettings);
    connect(loadSettings, &QPushButton::clicked, this, &DataQTO::loadSettingsSelected);
    hlaytmp->setAlignment(loadSettings,Qt::AlignLeft);

    saveSettings = new QPushButton("Сохранить");
    hlaytmp->addWidget(saveSettings);
    connect(saveSettings, &QPushButton::clicked, this, &DataQTO::saveSettingsSelected);
    hlaytmp->setAlignment(saveSettings,Qt::AlignLeft);

    vlay->addLayout(hlaytmp);

    vlay->setAlignment(hlaytmp,Qt::AlignLeft);

    connect(treeSelected, &QTreeWidget::itemChanged, this, &DataQTO::changetSetSelected);
    treeSelected->setHeaderLabels({"Параметр", "Описание", "Ось","Цвет","Привязка оси"});
    vlay->addWidget(treeSelected);
    cent->setLayout(vlay);
    selectedItemsDock->setWidget(cent);
}

void DataQTO::addNewSelected(QString Table, QString Name, QString Description)
{
    if(!Table.isEmpty() && !Name.isEmpty())
    {
        DataQTO::DataSelect selectItem;
        selectItem.nameParam = Name;
        selectItem.descParam = Description;
        //selectItem.axisSnap = 0;
        QComboBox *cb = new QComboBox;
        cb->setCurrentIndex(0);
        selectItem.axisSnap = cb;

        //selectItem.colorIndex = mDataSelected.size();
        CustomComboBox *BOX = new CustomComboBox;
        BOX->setCurrentIndex(mDataSelected.size());
        selectItem.colorBox = BOX;
        QCheckBox *checkRL = new QCheckBox;
        checkRL->setText("R[V]/L");
        selectItem.AlignRLAXISY = checkRL;

        int tableIndex = 0;
        if (!mDataSelected.isEmpty())
        {
            bool checktable = false;
            for(int i=0; i<mDataSelected.size(); i++)
            {
                DataQTO::mDataSelect data = mDataSelected.at(i);
                if(Table == data.nameTable)
                {
                    checktable = true;
                    bool checkfound = false;
                    for(int k=0; k<data.items.size(); k++)
                    {
                        DataQTO::DataSelect dataitem = data.items.at(k);
                        if(dataitem.nameParam == Name)
                        {
                            checkfound = true;
                            break;
                        }
                    }
                    if(!checkfound)
                    {
                        data.items.append(selectItem);
                        mDataSelected.replace(tableIndex,data);
                    }
                }
                tableIndex++;
            }
            if(!checktable)
            {
                DataQTO::mDataSelect tmptable;
                tmptable.nameTable = Table;
                QList<DataQTO::DataSelect> tmpitemselect;
                tmpitemselect.append(selectItem);
                tmptable.items = tmpitemselect;
                mDataSelected.append(tmptable);
            }
        }
        else
        {
            DataQTO::mDataSelect tmptable;
            tmptable.nameTable = Table;
            QList<DataQTO::DataSelect> tmpitemselect;
            tmpitemselect.append(selectItem);
            tmptable.items = tmpitemselect;
            mDataSelected.append(tmptable);
        }
    }

//    if(treeSelected)
//    {
//        for(int i=0; i<treeSelected->topLevelItemCount(); i++)
//        {
//            if(Table == treeSelected->topLevelItem(i)->text(0))
//            {
//                int col = 0;
//                for(int l=0; l<treeSelected->topLevelItem(i)->childCount(); l++)
//                {
//                    if(Name == treeSelected->topLevelItem(i)->child(l)->text(0))
//                    {
//                        col++;
//                        qDebug() << treeSelected->topLevelItem(i)->child(l)->text(0);
//                    }
//                }
//                if(col == treeSelected->topLevelItem(i)->childCount())
//                {
//                    QTreeWidgetItem *item = new QTreeWidgetItem;
//                    item->setText(0,Name);
//                    item->setText(1,Description);
//                    CustomComboBox *BOX = new CustomComboBox;
//                    QComboBox *cbcolor = BOX->getColorBox();;
//                    treeSelected->setItemWidget(item,3,cbcolor);
//                    QComboBox *cb = new QComboBox;
//                    cb->addItems(cbTexts);
//                    treeSelected->setItemWidget(item, 2, cb);
//                    treeSelected->topLevelItem(i)->addChild(item);
//                }
//            }
//        }
//    }

}

void DataQTO::removeSelected(QString Table, QString Name)
{
    QString tmpTable = Table.split("[")[0];
    for(int i=0; i<mDataSelected.size(); i++)
    {
        DataQTO::mDataSelect data = mDataSelected.at(i);
        if(tmpTable == data.nameTable)
        {
            for(int k=0; k<data.items.size(); k++)
            {
                DataQTO::DataSelect dataitem = data.items.at(k);
                if(dataitem.nameParam == Name)
                {
                    data.items.removeAt(k);
                    mDataSelected.replace(i,data);
                    if(mDataSelected.at(i).items.isEmpty())
                    {
                        mDataSelected.removeAt(i);
                    }
                    break;
                }
            }
        }
    }
}

void DataQTO::saveSettingsSelected()
{
    QString saving = QFileDialog::getSaveFileName();
    if(saving.contains(".select"))
    {
        saving.replace(".select","");
    }
    if(!saving.isEmpty())
    {
        QFile mFile(saving + ".select");
        if(mFile.exists())
        {
            mFile.remove();
        }
        mFile.open(QIODevice::Append | QIODevice::Text | QIODevice::ReadWrite);
        QTextStream out(&mFile);
        for(int i=0; i<mDataSelected.size(); i++)
        {
            QString forSave;
            QString nametables = mDataSelected.at(i).nameTable;
            forSave = nametables + "#:";
            for(int o=0; o<mDataSelected.at(i).items.size(); o++)
            {
                if(forSave.isEmpty())
                {
                    forSave = nametables + "#:";
                }
                forSave +=  mDataSelected.at(i).items.at(o).nameParam + "#:"
                        + mDataSelected.at(i).items.at(o).descParam + "#:"
                        + QVariant(mDataSelected.at(i).items.at(o).axisSnap->currentIndex()).toString() + "#:"
                        + QVariant(mDataSelected.at(i).items.at(o).colorBox->currentIndex()).toString() + "#:"
                        + QVariant(mDataSelected.at(i).items.at(o).AlignRLAXISY->checkState()).toString() + "\n";
                out << forSave;
                forSave.clear();
            }
        }
        mFile.close();
    }
}

void DataQTO::loadSettingsSelected()
{
    if(!mDataSelected.isEmpty())
    {
        mDataSelected.clear();
    }
    QString opend = QFileDialog::getOpenFileName();

    QFile f(opend);
    int n = 0;
    if (f.open(QIODevice::ReadOnly | QIODevice::Text))
       n = QTextStream(&f).readAll().split('\n').count();
    QList<QString> countitem;
    for(int i=0; i<n; i++) countitem.append(QVariant(i).toString());

    QFile file(opend);
    if(!opend.isEmpty())
    {
        if(file.open(QIODevice::ReadOnly |QIODevice::Text))
        {
            while(!file.atEnd())
            {
                QString str = file.readLine();
                str = str.replace("\n","");
                QStringList lst = str.split("#:");

                //=======
                DataQTO::DataSelect selectItem;
                selectItem.nameParam = lst[1];
                selectItem.descParam = lst[2];

                QComboBox *cb = new QComboBox;
                cb->addItems(countitem);
                cb->setCurrentIndex(QVariant(lst[3]).toInt());
                selectItem.axisSnap = cb;

                CustomComboBox *BOX = new CustomComboBox;
                BOX->setCurrentIndex(QVariant(lst[4]).toInt());
                selectItem.colorBox = BOX;

                QCheckBox *checkRL = new QCheckBox;
                checkRL->setText("R[V]/L");
                if(QVariant(lst[5]).toInt() == 2)
                {
                    checkRL->setCheckState(Qt::CheckState::Checked);
                }
                selectItem.AlignRLAXISY = checkRL;

                bool addselet = false;
                for(int i=0; i< mDataSelected.size(); i++)
                {
                    if(mDataSelected.at(i).nameTable == lst[0])
                    {
                        DataQTO::mDataSelect tmptable = mDataSelected.at(i);
                        QList<DataQTO::DataSelect> tmpitemselect = tmptable.items;
                        tmpitemselect.append(selectItem);
                        tmptable.items = tmpitemselect;
                        mDataSelected.replace(i,tmptable);
                        addselet = true;
                    }
                }

                if(!addselet)
                {
                    DataQTO::mDataSelect tmptable;
                    tmptable.nameTable = lst[0];
                    QList<DataQTO::DataSelect> tmpitemselect;
                    tmpitemselect.append(selectItem);
                    tmptable.items = tmpitemselect;
                    mDataSelected.append(tmptable);
                }
            }
        }
    }
    showSelected();
}

void DataQTO::changetSetSelected(QTreeWidgetItem *item, int column)
{
    Q_UNUSED(column);
    QTreeWidgetItem *tmpItem = static_cast<QTreeWidgetItem*>(item);
    for(int i=0; i<mDataSelected.size(); i++)
     {
        if(tmpItem->parent()->text(0) == mDataSelected.at(i).nameTable)
        {
            for(int k=0; k<mDataSelected.at(i).items.size(); k++)
            {
                QList<DataQTO::DataSelect> tmplistselect = mDataSelected.at(i).items;
                DataQTO::DataSelect tmpdata;
                tmpdata.nameParam = item->text(0);
                tmpdata.nameParam = item->text(1);
                QWidget *ttt = treeSelected->itemWidget(item,0);
                ttt->show();
            }
        }
    }
}
