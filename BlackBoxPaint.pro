#-------------------------------------------------
#
# Project created by QtCreator 2019-06-18T11:24:16
#
#-------------------------------------------------

QT       += core gui
QT       += network sql
QT       += opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = BlackBoxPaint
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += QCUSTOMPLOT_USE_OPENGL

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        delegate/customcombobox.cpp \
        graphicdata/build/dbtabletree.cpp \
        graphicdata/build/tabbuilder.cpp \
        graphicdata/graphicchart.cpp \
        graphicdata/qcustomplot.cpp \
        hideworkarea/monitorworker.cpp \
        main.cpp \
        sharedresources/configdata.cpp \
        sharedresources/dataqto.cpp \
        sharedresources/dataworkpaint.cpp \
        startwindow.cpp \
        workwindow.cpp

HEADERS += \
        delegate/customcombobox.h \
        graphicdata/build/dbtabletree.h \
        graphicdata/build/tabbuilder.h \
        graphicdata/graphicchart.h \
        graphicdata/qcustomplot.h \
        hideworkarea/monitorworker.h \
        sharedresources/configdata.h \
        sharedresources/dataqto.h \
        sharedresources/dataworkpaint.h \
        startwindow.h \
        workwindow.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    custom.qrc
