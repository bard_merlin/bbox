#include "startwindow.h"

int const StartWindow::EXIT_CODE_REBOOT = -123456789;

StartWindow::StartWindow()
{
    CFGD = new ConfigData;
    MDE = new DataQTO;

    nameConnect->setStyleSheet("background-color:white");
    ip->setStyleSheet("background-color:white");
    port->setStyleSheet("background-color:white");
    base->setStyleSheet("background-color:white");
    user->setStyleSheet("background-color:white");

    QHBoxLayout *allin = new QHBoxLayout;
    QVBoxLayout *vlayout = new QVBoxLayout;

    QHBoxLayout* hlayout = new QHBoxLayout;
    hlayout->addWidget(new QLabel("Имя сессии:"));
    hlayout->addWidget(nameConnect);
    nameConnect->setReadOnly(true);
    vlayout->addLayout(hlayout);

    hlayout = new QHBoxLayout;
    hlayout->addWidget(new QLabel("IP - Адрес:"));

    ip->setReadOnly(true);
    hlayout->addWidget(ip);
    vlayout->addLayout(hlayout);

    hlayout = new QHBoxLayout;
    hlayout->addWidget(new QLabel("Порт:"));
    hlayout->addWidget(port);
    port->setReadOnly(true);
    vlayout->addLayout(hlayout);

    hlayout = new QHBoxLayout;
    hlayout->addWidget(new QLabel("Имя Базы:"));
    hlayout->addWidget(base);
    base->setReadOnly(true);
    vlayout->addLayout(hlayout);

    hlayout = new QHBoxLayout;
    hlayout->addWidget(new QLabel("Пользователь:"));
    hlayout->addWidget(user);
    user->setReadOnly(true);
    vlayout->addLayout(hlayout);

    hlayout = new QHBoxLayout;
    hlayout->addWidget(new QLabel("Пароль:"));
    hlayout->addWidget(password);
    password->setReadOnly(true);
    vlayout->addLayout(hlayout);

    hlayout = new QHBoxLayout;
    connect(okbut,&QPushButton::clicked,this,&StartWindow::okAction);
    hlayout->addWidget(okbut);
    connect(cancelbut,&QPushButton::clicked,this,&StartWindow::cancelAction);
    hlayout->addWidget(cancelbut);
    vlayout->addLayout(hlayout);
    allin->addLayout(vlayout);

    vlayout = new QVBoxLayout;
    vlayout->addWidget(new QLabel("Соединения:"));
    listConnect = new QListWidget;
    listConnect->setStyleSheet("background-color: white;");
    vlayout->addWidget(listConnect);
    allin->addLayout(vlayout);

    setLayout(allin);

    listConnect->addItems(CFGD->getListConnection());
    listConnect->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(listConnect,&QListWidget::customContextMenuRequested,this,&StartWindow::prepareMenu);
    connect(listConnect,&QListWidget::itemClicked,this,&StartWindow::getSettings);
    connect(listConnect,&QListWidget::itemSelectionChanged,this,&StartWindow::setSelection);

    createWindowIN();

    QShortcut *enter = new QShortcut(this);
    enter->setKey(Qt::Key_Return);
    connect(enter,&QShortcut::activated,this,&StartWindow::okAction);
    enter = new QShortcut(this);
    enter->setKey(Qt::Key_Enter);
    connect(enter,&QShortcut::activated,this,&StartWindow::okAction);

    QShortcut *esc = new QShortcut(this);
    esc->setKey(Qt::CTRL + Qt::Key_Escape);
    connect(esc,&QShortcut::activated,this,&StartWindow::cancelAction);

    listConnect->setCurrentRow(0);
    emit listConnect->itemClicked(listConnect->currentItem());
}

StartWindow::~StartWindow(){}

void StartWindow::createWindowIN()
{
    addWidget = new QWidget;
    QPushButton *okbut = new QPushButton("Принять");
    QPushButton *cancelbut = new QPushButton("Отмена");

    QHBoxLayout *allin = new QHBoxLayout;
    QVBoxLayout *vlayout = new QVBoxLayout;

    QHBoxLayout* hlayout = new QHBoxLayout;
    hlayout->addWidget(new QLabel("Имя сессии:"));
    hlayout->addWidget(enameConnect);
    vlayout->addLayout(hlayout);

    hlayout = new QHBoxLayout;
    hlayout->addWidget(new QLabel("IP - Адрес:"));
    hlayout->addWidget(eip);
    //eip->setInputMask("999.999.999.999");
    vlayout->addLayout(hlayout);

    hlayout = new QHBoxLayout;
    hlayout->addWidget(new QLabel("Порт:"));
    hlayout->addWidget(eport);
    //eport->setInputMask("9999");
    vlayout->addLayout(hlayout);

    hlayout = new QHBoxLayout;
    hlayout->addWidget(new QLabel("Имя Базы:"));
    hlayout->addWidget(ebase);
    vlayout->addLayout(hlayout);

    hlayout = new QHBoxLayout;
    hlayout->addWidget(new QLabel("Пользователь:"));
    hlayout->addWidget(euser);
    vlayout->addLayout(hlayout);

    hlayout = new QHBoxLayout;
    hlayout->addWidget(new QLabel("Пароль:"));
    hlayout->addWidget(epassword);
    epassword->echoMode();
    vlayout->addLayout(hlayout);

    hlayout = new QHBoxLayout;
    connect(okbut,&QPushButton::clicked,this,&StartWindow::okConnectAdd);
    hlayout->addWidget(okbut);
    connect(cancelbut,&QPushButton::clicked,this,&StartWindow::cancelConnectAdd);
    hlayout->addWidget(cancelbut);
    vlayout->addLayout(hlayout);
    allin->addLayout(vlayout);
    addWidget->setLayout(allin);
}

void StartWindow::okAction()
{
    if(!ip->text().isEmpty() &&
       !port->text().isEmpty() &&
       !base->text().isEmpty() &&
       !user->text().isEmpty() &&
       !password->text().isEmpty())
    {
        CFGD->setNewConfig(nameConnect->text(),
                           "QPSQL",
                           ip->text(),
                           QVariant(port->text()).toInt(),
                           base->text(),
                           user->text(),
                           password->text());
        CFGD->saveSession();
    }
    else
    {
        CFGD->DefaultConfig("default");
    }

    bool checkconnect = CFGD->check_connectdb(CFGD->getCurrentConnect());

    if(checkconnect)
    {
        WorkWindow *mainwork = new WorkWindow();
        mainwork->showFullScreen();
        this->close();
    }
    else
    {
        QMessageBox *message = new QMessageBox;
        message->setWindowTitle("Ошибка соединения!");
        message->setText("<div> "
                         "<font size=\"7\"> (╯ </font>"
                         "<font color=\" red \" size=\"7\"> ° □ ° </font>"
                         "<font size=\"7\"> ）╯ </font> "
                         "<font color=\" black \" size=\"5\"> ︵ ┻━┻ </font> <br/> "
                         "</div>"
                         "<font size=\"4\"> Нет соединения с базой!</font> <br/>" + ip->text() + " " + port->text() + " " + base->text());
        message->setIcon(QMessageBox::Critical);
        message->addButton("Принять",QMessageBox::YesRole);
        message->show();
    }
}

void StartWindow::cancelAction()
{
    this->close();
}

void StartWindow::getSettings(QListWidgetItem *item)
{
    settingsConnect = CFGD->getConnect(item->text());
    nameConnect->setText(item->text());
    ip->setText(QVariant(settingsConnect->value("IP")).toString());
    port->setText(QVariant(settingsConnect->value("PORT")).toString());
    base->setText(QVariant(settingsConnect->value("BASE")).toString());
    user->setText(QVariant(settingsConnect->value("USER")).toString());
    password->setText(QVariant(settingsConnect->value("PASSWORD")).toString());
}

void StartWindow::deleteConnect()
{
    listConnect->removeItemWidget(listConnect->currentItem());
    CFGD->deleteConnect(listConnect->currentItem()->text());
    QApplication::exit(StartWindow::EXIT_CODE_REBOOT);
}

void StartWindow::addNewConnect()
{
    enameConnect->clear();
    eip->clear();
    eport->clear();
    ebase->clear();
    euser->clear();
    epassword->clear();
    addWidget->show();
}

void StartWindow::setSelection()
{
    emit listConnect->itemClicked(listConnect->currentItem());
}

void StartWindow::prepareMenu(const QPoint &pos)
{
    QListWidget *listconnect = listConnect;
    if(listconnect->itemAt(pos))
    {
        QMenu menu;
        QAction *newAct = new QAction("Редактировать");
        connect(newAct, &QAction::triggered, this, &StartWindow::editConnect);
        menu.addAction(newAct);
        newAct = new QAction("Удалить");
        connect(newAct, &QAction::triggered, this, &StartWindow::deleteConnect);
        menu.addAction(newAct);
        QPoint pt(pos);
        this->pt = pt;
        menu.exec(listconnect->mapToGlobal(pos));
    }
    else
    {
        QAction *newAct = new QAction("Добавить");
        connect(newAct, &QAction::triggered, this, &StartWindow::addNewConnect);
        QMenu menu(this);
        menu.addAction(newAct);
        QPoint pt(pos);
        this->pt = pt;
        menu.exec(listconnect->mapToGlobal(pos));
    }
}

void StartWindow::editConnect()
{
    QSettings *tmpset = CFGD->getConnect(listConnect->currentItem()->text());

    enameConnect->setText(listConnect->currentItem()->text());
    eip->setText(tmpset->value("IP").toString());
    eport->setText(tmpset->value("PORT").toString());
    ebase->setText(tmpset->value("BASE").toString());
    euser->setText(tmpset->value("USER").toString());
    epassword->setText((tmpset->value("PASSWORD")).toString());
    addWidget->show();
}

void StartWindow::okConnectAdd()
{
    if(!eip->text().isEmpty() &&
       !eport->text().isEmpty() &&
       !ebase->text().isEmpty() &&
       !euser->text().isEmpty() &&
       !epassword->text().isEmpty())
    {
        CFGD->setNewConfig(enameConnect->text(),
                           "QPSQL",
                           eip->text(),
                           QVariant(eport->text()).toInt(),
                           ebase->text(),
                           euser->text(),
                           epassword->text());
        CFGD->saveSession();
    }
    addWidget->close();
    QApplication::exit(StartWindow::EXIT_CODE_REBOOT);
}

void StartWindow::cancelConnectAdd()
{
    addWidget->close();
}
