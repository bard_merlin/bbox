#ifndef TABBUILDER_H
#define TABBUILDER_H

#include <QTabWidget>

#include <QWidget>
#include <QString>

class TabBuilder : public QTabWidget
{
public:
    TabBuilder(bool set_default);
    void newTab(QWidget *wigen, QString nametab);
    void resetWidgetTab(QWidget *wget, QString nametab);
};

#endif // TABBUILDER_H
