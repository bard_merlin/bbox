#include "dbtabletree.h"

DBTableTree::DBTableTree()
{
    //setSelectionMode(QAbstractItemView::NoSelection);
    //setFocusPolicy(Qt::NoFocus);

    setAnimated(true);
    setHeaderLabels({"БД/Таблица/Data:Имя","Описание"});//,"Привязка к оси"});
    mainBranch = new QTreeWidgetItem;
    mainBranch->setText(0,CFGD->getCurrentConnect());
    insertTopLevelItem(0,mainBranch);
    this->default_db = *CFGD->get_dbc();
    nameTable = getStringQuery(
                "SELECT table_name\n"
                "FROM information_schema.tables\n"
                "WHERE table_schema NOT IN ('information_schema','pg_catalog');",0);

    QMap<QString, QList<DBTableTree::dataTable>> data_Table;
    for(int i=0; i<nameTable.size(); i++)
    {
        QList<DBTableTree::dataTable> tableColumn = getColnameDesc(
                    "SELECT subq.attname, d.description \n"
                    "FROM(SELECT a.attname, c.oid, a.attnum \n"
                    "FROM pg_class c, pg_attribute a \n"
                    "WHERE c.oid = a.attrelid AND c.relname = '" + nameTable.at(i) + "'"
                                                                                     "AND a.attnum > 0) \n subq "
                                                                                     "LEFT OUTER JOIN pg_description d \n"
                                                                                     "ON(d.objsubid = subq.attnum AND d.objoid = subq.oid);");
        data_Table.insert(nameTable.at(i),tableColumn);
    }
    setTreeData(data_Table);
    expandAll();
    resizeColumnToContents(0);
    collapseAll();
}

void DBTableTree::setTreeData(QMap<QString, QList<DBTableTree::dataTable>> data_table)
{
    for(int i=0; i<data_table.size(); i++)
    {
        QTreeWidgetItem* gett = convertChild(nameTable.at(i) + "[" + QVariant(data_table.value(nameTable.at(i)).size()+1).toString() + "]");
        mainBranch->addChild(gett);
        for(int k=0; k<data_table.value(nameTable.at(i)).size(); k++)
        {
            QTreeWidgetItem* symlink = convertChild(data_table.value(nameTable.at(i)).at(k).name,data_table.value(nameTable.at(i)).at(k).description);
            gett->insertChild(0,symlink);
        }
    }
}

void DBTableTree::visitTree(QList<QTreeWidgetItem> &list, QTreeWidgetItem *item)
{
    list << *item;
    for(int i=0;i<item->childCount(); i++)
    {
        visitTree(list, item->child(i));
    }
}

QVector<QString> DBTableTree::getStringQuery(QString query, int column)
{
    QVector<QString> tabledata;
    if (default_db.isOpen())
    {
        QSqlQuery cquery(default_db);
        cquery.exec(query);
        QVector<QString> rowdata;
        while (cquery.next())
        {
            rowdata.append(cquery.value(column).toString());
        }
        tabledata.append(rowdata);
        return tabledata;
    }
    else
    {
        qDebug() << "noConnected WorkerInst";
        return tabledata;
    }
}

QList<DBTableTree::dataTable> DBTableTree::getColnameDesc(QString query)
{
    QList<DBTableTree::dataTable> tmpdatatable;
    if (default_db.isOpen())
    {
        QSqlQuery cquery(default_db);
        cquery.exec(query);

        QList<QString> namedata;
        cquery.next();
        DBTableTree::dataTable tmpdata;
        while (cquery.next())
        {
            tmpdata.name = cquery.value(0).toString();
            tmpdata.description = cquery.value(1).toString();
            tmpdatatable.append(tmpdata);
        }
        return tmpdatatable;
    }
    else
    {
        qDebug() << "noConnected " << default_db.databaseName();
        return tmpdatatable;
    }
}

QList<DBTableTree::base> DBTableTree::visitTree(QTreeWidget *tree)
{
    QList<DBTableTree::base> bases;
    for(int i=0; i<tree->topLevelItemCount(); i++)
    {
        base base;
        base.name = tree->topLevelItem(i)->text(0);
        QList<base::table> tables;
        for(int l=0;  l<tree->topLevelItem(i)->childCount(); l++)
        {
            base::table table;
            table.name = tree->topLevelItem(i)->child(l)->text(0);
            visitTree(table.items, tree->topLevelItem(i)->child(l));
            tables.append(table);
        }
        base.tables = tables;
        bases.append(base);
    }
    return bases;
}

void DBTableTree::dropALLSelected()
{
    for(int l=0; l< mainBranch->childCount(); l++)
    {
        mainBranch->child(l)->setCheckState(0,Qt::CheckState::Unchecked);
        for(int k=0; k< mainBranch->child(l)->childCount(); k++)
        {
            mainBranch->child(l)->child(k)->setCheckState(0,Qt::CheckState::Unchecked);
        }
    }
}

QTreeWidgetItem *DBTableTree::convertChild(QString nameChild,QString description)
{
    QTreeWidgetItem *item = new QTreeWidgetItem;
    item->setCheckState(0,Qt::CheckState::Unchecked);
    item->setText(0,nameChild);
    item->setText(1,description);
    return item;
}

QList<QTreeWidgetItem *> DBTableTree::convertChildList(QList<QString> nameChild)
{
    QList<QTreeWidgetItem *> listChild;
    for(int i=0; i<nameChild.size(); i++)
    {
        QTreeWidgetItem *item = new QTreeWidgetItem;
        item->setCheckState(0,Qt::CheckState::Unchecked);
        item->setText(0,nameChild.at(i));
        listChild.append(item);
    }
    return  listChild;
}
