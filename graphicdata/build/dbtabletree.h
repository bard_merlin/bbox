#ifndef DBTABLETREE_H
#define DBTABLETREE_H

#include <QTreeWidget>

#include "hideworkarea/monitorworker.h"

#include "sharedresources/configdata.h"
extern ConfigData *CFGD;

class DBTableTree : public QTreeWidget
{   
public:
    struct base
    {
        QString name;
        struct table
        {
            QString name;
            QList<QTreeWidgetItem> items;
        };
        QList<table> tables;
    };

    struct dataTable
    {
        QString name;
        QString description;
    };


public:
    DBTableTree();
    void setTreeData(QMap<QString, QList<DBTableTree::dataTable>> data_table);
    QList<DBTableTree::base> visitTree(QTreeWidget *tree);
    void dropALLSelected();

private:
    QSqlDatabase default_db;
    QVector<QString> nameTable;
    QTreeWidgetItem *mainBranch;
    QTreeWidgetItem *convertChild(QString nameChild, QString description = nullptr);
    QList<QTreeWidgetItem *> convertChildList(QList<QString> nameChild);
    void visitTree(QList<QTreeWidgetItem> &list, QTreeWidgetItem *item);

    QVector<QString> getStringQuery(QString query, int column);
    QList<DBTableTree::dataTable> getColnameDesc(QString query);

private slots:
    void setchekeditem(QTreeWidgetItem *item, int column);
};

#endif // DBTABLETREE_H
