#include "tabbuilder.h"

TabBuilder::TabBuilder(bool set_default)
{
    if(set_default)
    {
        QWidget *tmpwidget = new QWidget;
        tmpwidget->setStyleSheet("background-color:lightgray");
        newTab(tmpwidget,"default");
    }
}

void TabBuilder::newTab(QWidget *wigen, QString nametab)
{
    wigen->setStyleSheet("background-color:lightgray");
    addTab(wigen,nametab);
}

void TabBuilder::resetWidgetTab(QWidget *wget, QString nametab)
{
    this->removeTab(0);
    addTab(wget,nametab);
}
