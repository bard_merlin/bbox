﻿#ifndef GRAPHICCHART_H
#define GRAPHICCHART_H

#include <QMutex>

#include <QtOpenGL/QtOpenGL>

#include "graphicdata/qcustomplot.h"

class GraphicChart : public QCustomPlot
{
    Q_OBJECT
    struct dataPaint
    {
        QString namedataline;
        QVector<double> key;
        QVector<double> value;
    };

public:
    GraphicChart();
    void newSeries(QString name,QString color,int leftright, int axis, QString nameAXIS);
    void newLine(QString name, QString color);
    void addNewData(QString nameLine, QVector<double> key, QVector<double> data);
    void clearPlot();
    void setupLenend(const QCustomPlot *customPlot) const;
    void setRangeX(double minimum, double maximum);
    void restore_defAXIS();
    QList<QCPGraph *> getListGraph();

//public: signals:
    //void chartdone(QString datareadwrite);

private:
    QColor color;
    QString name = "null";
    //int flat = 0;
    QCPAxis* storedYAX;

private:
    QTimer *timeToPaint;
    QQueue<GraphicChart::dataPaint> queuePaint;
    QList<QCPGraph*> dataseries;
    QList<QCPAxis*> dataaxis;
    QMutex mutex;
    QString qformat = "yyyy-MM-dd HH:mm:ss.zzz";

private slots:
    void paintQueueData();
    void storeDataSeries(QCPAbstractLegendItem *item);
    void restoreDataSeries(QCPAbstractLegendItem *item);
    void setSelectedSeries(QCPLegend *legend, QCPAbstractLegendItem *item);
};

#endif // GRAPHICCHART_H
