#include "graphicchart.h"

GraphicChart::GraphicChart()
{
    xAxis->setTickLabelFont(QFont(QFont().family(), 8));
    //yAxis->setTickLabelFont(QFont(QFont().family(), 8));
    yAxis->setVisible(false);

    legend->setVisible(true);
    axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignLeft|Qt::AlignBottom);

    setupLenend(this);
    xAxis->setTickLabelType(QCPAxis::ltDateTime);
    xAxis->setDateTimeFormat("dd-MM-yyyy\nhh:mm:ss:zzz");

    timeToPaint = new QTimer;
    connect(timeToPaint,&QTimer::timeout,this,&GraphicChart::paintQueueData);
    timeToPaint->setInterval(100);

    connect(this,&QCustomPlot::legendClick,this,&GraphicChart::setSelectedSeries);
    //setOpenGl(true);

    storedYAX = yAxis;

    ///////////////////////////////////////////////////////////////////
}

void GraphicChart::newSeries(QString name, QString color, int leftright, int axis, QString nameAXIS)
{
    bool existthis = false;

    QCPAxis* axiy = yAxis;
    for(int i=0; i<dataaxis.size(); i++)
    {
        if(QVariant(axis).toString() + "[" + QVariant(leftright).toString() + "]" == dataaxis.at(i)->objectName())
        {
            existthis = true;
            axiy = dataaxis.at(i);
        }
    }

    if(!existthis)
    {
        if(leftright == 2)
        {
            axiy = axisRect()->addAxis(QCPAxis::atRight);
            axiy->setObjectName(QVariant(axis).toString() + "[" + QVariant(leftright).toString() + "]");
        }
        if(leftright == 0)
        {
            axiy = axisRect()->addAxis(QCPAxis::atLeft);
            axiy->setObjectName(QVariant(axis).toString() + "[" + QVariant(leftright).toString() + "]");
        }
    }
    if(nameAXIS != "")
    {
        axiy->setLabel(nameAXIS);
    }
    else
        axiy->setLabel(QVariant(axis).toString() + "[" + QVariant(leftright).toString() + "]");

    axiy->setBasePen(QPen(QBrush(QColor(color)),2));
    QCPGraph* graph = addGraph(xAxis, axiy);
    graph->setName(name);
    graph->setObjectName(name.replace("\n",""));
    graph->setPen(QPen(color));
    dataseries.append(graph);
    dataaxis.append(axiy);

    //qDebug() << "Series: " << graph->objectName() << "axY: " << axiy->objectName();
}

void GraphicChart::newLine(QString name, QString color)
{
    QCPGraph* graph = addGraph();
    graph->setName(name);
    graph->setPen(QPen(color));
    dataseries.append(graph);
}

void GraphicChart::addNewData(QString nameLine, QVector<double> key, QVector<double> data)
{   
    //qDebug() << "in THE PAINT" << nameLine << data.size();
    if(!key.isEmpty() && !data.isEmpty() && !nameLine.isEmpty())
    {
        GraphicChart::dataPaint paintdata;
        paintdata.key = key;
        paintdata.value = data;
        paintdata.namedataline = nameLine;
        //qDebug() << key << data << nameLine << key.size();
        if((key.size()>0 && data.size()>0) && (key.size() == data.size()))
        {
            mutex.lock();
            //setRangeX(key.at(0),key.at(key.size()-1));
            queuePaint.enqueue(paintdata);
            mutex.unlock();
        }
    }
    timeToPaint->start();
}

void GraphicChart::clearPlot()
{
    if(!dataseries.isEmpty())
    {
        for(int i=0; i<dataseries.size(); i++)
        {
            dataseries.at(i)->clearData();
            axisRect()->removeAxis(dataaxis.at(i));
        }
        dataseries.clear();
        dataaxis.clear();

        this->clearPlot();
        this->clearGraphs();
        replot();
    }
}

void GraphicChart::setupLenend(const QCustomPlot *customPlot) const
{
    QCPLayoutGrid *subLayout = new QCPLayoutGrid;
    customPlot->plotLayout()->addElement(1, 0, subLayout);
    subLayout->addElement(0, 0, new QCPLayoutElement);
    subLayout->addElement(1, 0, customPlot->legend);
    subLayout->addElement(0, 1, new QCPLayoutElement);
    customPlot->plotLayout()->setRowStretchFactor(1, 0.0001);
}

void GraphicChart::setRangeX(double minimum, double maximum)
{
    xAxis->setRange(minimum,maximum);
}

void GraphicChart::restore_defAXIS()
{
    yAxis = storedYAX;
}

QList<QCPGraph *> GraphicChart::getListGraph()
{
    return dataseries;
}

void GraphicChart::paintQueueData()
{
    while (!queuePaint.isEmpty())
    {
        GraphicChart::dataPaint paintdata;
        mutex.lock();
        paintdata = queuePaint.dequeue();
        //qDebug() << "key: " << paintdata.key << endl << "value: " << paintdata.value << endl;

        for(int i=0; i<dataseries.size(); i++)
        {
            if(dataseries.at(i)->objectName() == paintdata.namedataline.replace("\n",""))
            {
                for(int b=0; b<paintdata.key.size();b++)
                {
                    dataseries.at(i)->addData(QCPData(paintdata.key.at(b)/*/1000*/,paintdata.value.at(b)));
                    //reset
//                    if(flat==50)
//                    {
//                        QCPItemText *phaseTracerText = new QCPItemText(this);
//                        phaseTracerText->position->setType(QCPItemPosition::ptAxisRectRatio);
//                        phaseTracerText->setPositionAlignment(Qt::AlignRight|Qt::AlignBottom);
//                        phaseTracerText->position->setCoords(1.0, 0.95);
//                        phaseTracerText->setText(QString("[%2]\n%1").arg(paintdata.key.at(b)).arg(paintdata.value.at(b)));
//                        phaseTracerText->setTextAlignment(Qt::AlignLeft);
//                        phaseTracerText->setFont(QFont(font().family(), 9));
//                        phaseTracerText->setPadding(QMargins(8, 0, 0, 0));
//                    }
//                        QCPItemTracer *phaseTracer = new QCPItemTracer(this);
//                        phaseTracer->setGraph(dataseries.at(i));
//                        phaseTracer->setGraphKey(paintdata.key.at(b));
//                        phaseTracer->setInterpolating(true);
//                        phaseTracer->setStyle(QCPItemTracer::tsCircle);
//                        phaseTracer->setPen(QPen(Qt::red));
//                        phaseTracer->setBrush(dataseries.at(i)->pen().color());
//                        phaseTracer->setSize(5);
//                        flat = 0;
//                    }
//                    flat++;
                    //reset
                    //dataaxis.at(i)->rescale();
                }
            }
        }

        replot();
        //yAxis->rescale();
        rescaleAxes();
        mutex.unlock();
        timeToPaint->stop();
    }

    ///QQQQQQQQQQQQ!!!!!!!!!!!!
//    if(queuePaint.isEmpty())
//    {
//        emit chartdone("График построен!");
//    }
}

void GraphicChart::storeDataSeries(QCPAbstractLegendItem *item)
{
    QCPPlottableLegendItem *plItem = qobject_cast<QCPPlottableLegendItem*>(item);
    int k=0;
    for(int i=0; i<dataseries.size(); i++)
    {
        if(dataseries.at(i)->name() != plItem->plottable()->name())
        {
            dataseries.at(i)->setVisible(false);
        }
        else
        {
            //qDebug() << dataaxis.at(k)->objectName() << dataseries.at(i)->name();
            k=i; 
        }
    }

    for(int i=0; i<dataaxis.size(); i++)
    {
        if(i!=k && dataaxis.at(i)->objectName() != dataaxis.at(k)->objectName())
        {
            dataaxis.at(i)->setVisible(false);
        }
    }

    yAxis = dataaxis.at(k);
    dataaxis.at(k)->setBasePen(QPen(QBrush(QColor("red")),2));

    color = plItem->plottable()->pen().color();
    name = plItem->plottable()->name();
}

void GraphicChart::restoreDataSeries(QCPAbstractLegendItem *item)
{
    QCPPlottableLegendItem *plItem = qobject_cast<QCPPlottableLegendItem*>(item);
    QPen *pen = new QPen(color);
    plItem->plottable()->setPen(*pen);
    name.clear();
    for(int i=0; i<dataseries.size(); i++)
    {
        dataseries.at(i)->setVisible(true);
        if(dataaxis.at(i)->basePen().color() == QColor("red"))
            dataaxis.at(i)->setBasePen(QPen(QBrush(QColor(color)),2));
    }

    for(int i=0; i<dataaxis.size(); i++)
    {
        dataaxis.at(i)->setVisible(true);
    }
}

void GraphicChart::setSelectedSeries(QCPLegend *legend, QCPAbstractLegendItem *item)
{
    bool clearitem = false;
    if(item)
    {
        QCPPlottableLegendItem *plItem = qobject_cast<QCPPlottableLegendItem*>(item);
        if(plItem->plottable()->name() == name)
        {
            clearitem = true;
        }

        QCPLegend *plLegend = qobject_cast<QCPLegend*>(legend);
        for(int i=0; i<plLegend->itemCount(); i++)
        {
            QCPPlottableLegendItem *tmpItem = qobject_cast<QCPPlottableLegendItem*>(plLegend->item(i));
            if(tmpItem->plottable()->name() == name)
            {
                restoreDataSeries(tmpItem);
                tmpItem->setSelected(false);
            }
        }

        if(!plItem->selected())
        {
            if(!clearitem)
            {
                storeDataSeries(plItem);
                QPen *pen = new QPen("red");
                plItem->plottable()->setPen(*pen);
                plItem->setSelected(true);
            }
            else
            {
                restoreDataSeries(plItem);
                plItem->setSelected(false);
            }
        }
        else
        {
            restoreDataSeries(plItem);
        }
        this->replot();
    }
}
