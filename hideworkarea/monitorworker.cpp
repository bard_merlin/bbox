#include "monitorworker.h"

//instanceworker
WorkerInstance::WorkerInstance(QString nameInstance, QSettings *settings, void *pmon)
{
    qRegisterMetaType < QVector<QVector<double>> >() ;
    qRegisterMetaType < QList<QString> >();
    this->typeDB = settings->value("TYPE").toString();
    this->namedb = settings->value("BASE").toString();
    this->hostname = settings->value("IP").toString();
    this->port = settings->value("PORT").toInt();
    this->user = settings->value("USER").toString();
    this->password = settings->value("PASSWORD").toString();
    this->dmain = pmon;
    this->nameInstance = nameInstance;
    this->freeoutstatus = false;
    connectInstance();
}

WorkerInstance::~WorkerInstance()
{
    base.close();
    QSqlDatabase::removeDatabase(nameInstance);
    //base.~QSqlDatabase();
}

void WorkerInstance::run()
{
    MonitorWorker *many = static_cast<MonitorWorker*>(dmain);

    while(true)
    {
        many->mutex.lock();
        QVector<double> timeData;
        QVector<QVector<double>> dataOutme;
        QString nameTable;
        QList<QString> mnameData;
        if(!many->taskQueue.isEmpty())
        {
            QString data = many->taskQueue.dequeue();
            emit stepProgress(many->taskQueue.size());
            //выгрузка из базы ***************************************
            if (base.isOpen())
            {
                int records = 0;
                QString tmpquery = data;
                tmpquery.replace(";","");
                QString reformQuery = "SELECT COUNT(countRecords) FROM ( " + tmpquery + " ) AS countRecords;";
                //qDebug() << "Выполняю [" << nameInstance << "]:" << reformQuery;
                records = getCountRecords(reformQuery);
                if(!(records == 0))
                {
                    MDE->addToTotalSize(records);
                }
                //разборка имён
                QString tmpquerysplit = data;
                auto dataspmlit = tmpquerysplit.split("WHERE")[0].split("FROM");
                nameTable = dataspmlit[1].replace(" ", "");
                QString tmpsplit = dataspmlit[0].replace("SELECT tm, ", "");
                auto names = tmpsplit.split(", ");
                foreach(QString name, names)
                {
                    mnameData.append(name.replace(" ", ""));
                }
                //qDebug() << "Таблица:" << nameTable << "Переменные:" << mnameData;

                //reformdata **************************************************************8
                timeData.resize(records);

                QSqlQuery cquery(base);
                cquery.exec(data);
                int i=0;
                while (cquery.next())
                {
                    timeData[i] = QDateTime::fromString(cquery.value(0).toString(), "yyyy-MM-ddTHH:mm:ss.zzz").toMSecsSinceEpoch();
                    i++;
                }

                for(int c=0; c<mnameData.size(); c++)
                {
                    i=0;

                    cquery.first();
                    cquery.previous();

                    QVector<double> outData(records);
                    while (cquery.next())
                    {
                        outData[i] = cquery.value(c+1).toDouble();
                        i++;
                    }
                    if(!outData.isEmpty())
                        dataOutme.append(outData);
                }
            }
            else
            {
                qDebug() << "noConnected " << nameInstance;
                many->mutex.unlock();
                break;
            }
            //выгрузка из базы ***************************************

            if(!data.isEmpty())
            {
                if(!nameTable.isEmpty() && !timeData.isEmpty() && !mnameData.isEmpty() && !dataOutme.isEmpty())
                {
                    emit dataout(nameTable, timeData, mnameData, dataOutme);
                    emit nameWorkerWork("Получено :" +  QVariant(timeData.size()).toString());
                }
                else
                {
                     emit nameWorkerWork("Получено : NULL");
                }
            }
            many->mutex.unlock();
        }
        else
        {
            many->mutex.unlock();
            break;
        }
    }
    qDebug() << nameInstance  << " (<!>) свободен";
    emit freethread();
}

void WorkerInstance::connectInstance()
{
    base = QSqlDatabase::addDatabase(typeDB,nameInstance);
    base.setDatabaseName(namedb);
    base.setHostName(hostname);
    base.setPort(port);
    base.setUserName(user);
    base.setPassword(password);
    bool ok = base.open();
    if(ok)
    {
        qDebug() << "База:" << nameInstance << "isConnected";
    }
    else
    {
        qDebug() << "База:" << nameInstance << "noConnected";
    }
}

int WorkerInstance::getCountRecords(QString query)
{
    int countRecords = 0;
    if (base.isOpen())
    {
        QSqlQuery cquery(base);
        cquery.exec(query);

        while (cquery.next())
        {
            countRecords = cquery.value(0).toInt();
        }
        return countRecords;
    }
    else
    {
        qDebug() << "noConnected " << nameInstance;
        return countRecords;
    }
}

//=============================================================================================================================================
//*********************************************************************************************************************************************
//=============================================================================================================================================

//monitorworker
MonitorWorker::MonitorWorker(QString dataoftime, QString datatotime, DataWorkPaint *datawork)
{
    this->oftime = dataoftime;
    this->totime = datatotime;
    this->datawork = datawork;
    instanceInWorkCount = 0;
}

QList<MonitorWorker::dataCutTime> MonitorWorker::splitTime()
{
    QList<dataCutTime> timelist;
    double splittime = CFGD->getTimeSplit() * 60/*min*/ * 1000/*ms*/;
    QDateTime oftm = QDateTime::fromString(oftime, qformat);
    double doftm = oftm.toMSecsSinceEpoch();
    QDateTime totm = QDateTime::fromString(totime, qformat);
    double dtotm = totm.toMSecsSinceEpoch();

    MonitorWorker::dataCutTime dCT;
    dCT.oftime = QDateTime::fromMSecsSinceEpoch(qint64(doftm)).toString(qformat);
    if((doftm + splittime) > dtotm)
    {
        dCT.totime = QDateTime::fromMSecsSinceEpoch(qint64(dtotm)).toString(qformat);
    }
    else
        dCT.totime = QDateTime::fromMSecsSinceEpoch(qint64(doftm + splittime)).toString(qformat);
    timelist.append(dCT);

    double slitime = doftm + splittime;
    do
    {
        MonitorWorker::dataCutTime dCT;
        if(slitime < dtotm)
        {
            dCT.oftime = QDateTime::fromMSecsSinceEpoch(qint64(slitime + 1)).toString(qformat);
            slitime = slitime + splittime;
            if(slitime > dtotm)
            {
                dCT.totime = QDateTime::fromMSecsSinceEpoch(qint64(dtotm)).toString(qformat);
            }
            else
            {
                dCT.totime = QDateTime::fromMSecsSinceEpoch(qint64(slitime)).toString(qformat);
            }
            timelist.append(dCT);
        }
    }
    while(slitime < dtotm);

    return timelist;
}

void MonitorWorker::startMonitor()
{
    emit stepStatus("Формирую воркеры");
    for(int i=0; i<CFGD->getWorkerCount(); i++)
    {
        WorkerInstance *worker = new WorkerInstance("worker_isID0x" + QVariant(i).toString(), CFGD->getConnect(CFGD->getCurrentConnect()) , this);
        connect(worker,&WorkerInstance::dataout,this,&MonitorWorker::outdata);
        connect(worker,&WorkerInstance::freethread,this,&MonitorWorker::freethreadaction);
        connect(worker,&WorkerInstance::stepProgress,this,&MonitorWorker::stepNowProgressQueue);
        connect(worker,&WorkerInstance::nameWorkerWork,this,&MonitorWorker::askWhatWorker);
        listworkers.append(worker);
        instanceInWorkCount++;
    }
    emit stepStatus("Формирую очередь");
    formqueue();
}

void MonitorWorker::formqueue()
{
    mutex.lock();
    timereformed = splitTime();
    datatmpquery = *MDE->getMDataSelected();
    foreach(MonitorWorker::dataCutTime timecut, timereformed)
    {
        QString tmpquery = query;
        tmpquery.replace("%OfTime%",timecut.oftime);
        tmpquery.replace("%ToTime%",timecut.totime);
        for(int i=0; i<datatmpquery.size(); i++)
        {
            QString retmpquery = tmpquery;
            retmpquery.replace("%nameTable%", datatmpquery.at(i).nameTable);
            QString tmpname = "tm";
            for(int k=0; k<datatmpquery.at(i).items.size(); k++)
            {
                tmpname = tmpname + ", " + datatmpquery.at(i).items.at(k).nameParam;
            }
            retmpquery.replace("%nameParam%", tmpname);
            taskQueue.enqueue(retmpquery);
        }
    }
    mutex.unlock();

    emit stepStatus("<!> Элементов в очереди: " + QVariant(taskQueue.size()).toString());
    emit stepStatus("Запускаю воркеры");
    emit setRangeProgressQueue(taskQueue.size());

    if(taskQueue.size()>0)
    {
        for(int i=0; i<listworkers.size(); i++)
        {
            listworkers.at(i)->start();
        }
    }
    emit stepStatus("Воркеры запущены");
}

void MonitorWorker::outdata(QString nameTable, QVector<double> keyData, QList<QString> nameParams, QVector<QVector<double>> ValueData)
{
    datawork->enqueuePaint(nameTable, keyData, nameParams, ValueData);
}

void MonitorWorker::stepNowProgressQueue(int dvalue)
{
    emit setValueProgressQueue(dvalue);
}

void MonitorWorker::askWhatWorker(QString nameworker)
{
    emit statusWorkMain(nameworker);
}

void MonitorWorker::freethreadaction()
{
    this->mutex.lock();
    instanceInWorkCount--;
    if(instanceInWorkCount == 0)
    {
         emit stepStatus("Очередь пуста");
         foreach(WorkerInstance *instance, listworkers)
         {
             disconnect(instance, &WorkerInstance::dataout,this,&MonitorWorker::outdata);
             disconnect(instance,&WorkerInstance::freethread,this,&MonitorWorker::freethreadaction);
             disconnect(instance,&WorkerInstance::stepProgress,this,&MonitorWorker::stepNowProgressQueue);
             disconnect(instance,&WorkerInstance::nameWorkerWork,this,&MonitorWorker::askWhatWorker);
             delete instance;
         }
         listworkers.clear();
         emit stepStatus("Воркеры удалены");
         emit stepStatus("Всего записей: " + QVariant(MDE->getTotalSize()).toString());
         emit statusWorkMain("Чтение завершено!");
    }
    this->mutex.unlock();
}

//=============================================================================================================================================
//*********************************************************************************************************************************************
//=============================================================================================================================================

RealTimeInstance::RealTimeInstance(QString QueryRealTime, QString nameInstance, QSettings *settings)
{
    qDebug() << "CREATED " << nameInstance;
    qRegisterMetaType < QVector<QVector<double>> >() ;
    qRegisterMetaType < QList<QString> >();
    this->typeDB = settings->value("TYPE").toString();
    this->namedb = settings->value("BASE").toString();
    this->hostname = settings->value("IP").toString();
    this->port = settings->value("PORT").toInt();
    this->user = settings->value("USER").toString();
    this->password = settings->value("PASSWORD").toString();
    this->nameInstance = nameInstance;
    this->freeoutstatus = false;
    this->RealTimeQuery = QueryRealTime;
    connectInstance();
}

void RealTimeInstance::run()
{
    //qDebug() << "RUNNED" << QThread::currentThread() << nameInstance << RealTimeQuery;
    while(!stopsing)
    {
        QString maintmpquery = RealTimeQuery;

        QString nameTable;
        QList<QString> mnameData;

        QString tmpquerysplit = maintmpquery;
        auto dataspmlit = tmpquerysplit.split("WHERE")[0].split("FROM");
        nameTable = dataspmlit[1].replace(" ", "");
        QString tmpsplit = dataspmlit[0].replace("SELECT tm, ", "");
        auto names = tmpsplit.split(", ");
        foreach(QString name, names)
        {
            mnameData.append(name.replace(" ", ""));
        }

        QString time_now = getTimeReal(nameTable);

        //qDebug() << time_now;

        QDateTime totm = QDateTime::fromString(time_now, "yyyy-MM-ddTHH:mm:ss.zzz");
        double doftm = totm.toMSecsSinceEpoch();

        //qDebug() << QVariant(timememe).toString();
        if(QVariant(timememe).toString() == "0")
        {
            maintmpquery.replace("%OfTime%",QDateTime::fromMSecsSinceEpoch(qint64(doftm - 10000)).toString(qformat));
            //qDebug() << "full";
            timememe = doftm;
        }
        else
        {
            maintmpquery.replace("%OfTime%",QDateTime::fromMSecsSinceEpoch(qint64(timememe)).toString(qformat));
            //qDebug() << "[" << QVariant(tick).toString() << "]" << "OF:" << QDateTime::fromMSecsSinceEpoch(qint64(timememe)).toString(qformat) << " TO:" << QDateTime::fromMSecsSinceEpoch(qint64(doftm)).toString(qformat);
            //qDebug() << "doo it";
            timememe = doftm;
        }
        maintmpquery.replace("%ToTime%",QDateTime::fromMSecsSinceEpoch(qint64(doftm)).toString(qformat));

        //qDebug() << "[" << QVariant(tick).toString() << "]" << maintmpquery;
        tick++;

        QVector<QVector<double>> dataOutme;
        QVector<double> timeData;
        QString tmpquery = maintmpquery;
        tmpquery.replace(";","");
        QString reformQuery = "SELECT COUNT(countRecords) FROM ( " + tmpquery + " ) AS countRecords;";
        //qDebug() << reformQuery;
        int records = getCountRecords(reformQuery);
        timeData.resize(records);

        //qDebug() << QVariant(records).toString();
        if (base.isOpen())
        {
            QSqlQuery cquery(base);
            cquery.exec(maintmpquery);
            int i=0;
            while (cquery.next())
            {
                timeData[i] = QDateTime::fromString(cquery.value(0).toString(), "yyyy-MM-ddTHH:mm:ss.zzz").toMSecsSinceEpoch();
                i++;
            }

            for(int c=0; c<mnameData.size(); c++)
            {
                i=0;

                cquery.first();
                cquery.previous();

                QVector<double> outData(records);
                while (cquery.next())
                {
                    outData[i] = cquery.value(c+1).toDouble();
                    i++;
                }
                if(!outData.isEmpty())
                    dataOutme.append(outData);
            }
        }
        //qDebug() << nameTable << timeData << mnameData << dataOutme;
        emit dataout(nameTable, timeData, mnameData, dataOutme);
        msleep(1000);

//            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//            //разборка имён
//            QString tmpquerysplit = maintmpquery;
//            auto dataspmlit = tmpquerysplit.split("WHERE")[0].split("FROM");
//            nameTable = dataspmlit[1].replace(" ", "");
//            QString tmpsplit = dataspmlit[0].replace("SELECT tm, ", "");
//            auto names = tmpsplit.split(", ");
//            foreach(QString name, names)
//            {
//                mnameData.append(name.replace(" ", ""));
//            }
//            //qDebug() << "Таблица:" << nameTable << "Переменные:" << mnameData;

//            //reformdata **************************************************************8
//            timeData.resize(records);

//            QSqlQuery cquery(base);
//            cquery.exec(data);
//            int i=0;
//            while (cquery.next())
//            {
//                timeData[i] = QDateTime::fromString(cquery.value(0).toString(), "yyyy-MM-ddTHH:mm:ss.zzz").toMSecsSinceEpoch();
//                i++;
//            }

//            for(int c=0; c<mnameData.size(); c++)
//            {
//                i=0;

//                cquery.first();
//                cquery.previous();

//                QVector<double> outData(records);
//                while (cquery.next())
//                {
//                    outData[i] = cquery.value(c+1).toDouble();
//                    i++;
//                }
//                if(!outData.isEmpty())
//                    dataOutme.append(outData);
//            }
//        }
//        else
//        {
//            qDebug() << "noConnected " << nameInstance;
//            many->mutex.unlock();
//            break;
//        }
//        //выгрузка из базы ***************************************

//        if(!data.isEmpty())
//        {
//            if(!nameTable.isEmpty() && !timeData.isEmpty() && !mnameData.isEmpty() && !dataOutme.isEmpty())
//            {
//                emit dataout(nameTable, timeData, mnameData, dataOutme);
//                emit nameWorkerWork("Получено :" +  QVariant(timeData.size()).toString());
//            }
//            else
//            {
//                 emit nameWorkerWork("Получено : NULL");
//            }
//        }
    }
}

void RealTimeInstance::connectInstance()
{
    base = QSqlDatabase::addDatabase(typeDB,nameInstance);
    base.setDatabaseName(namedb);
    base.setHostName(hostname);
    base.setPort(port);
    base.setUserName(user);
    base.setPassword(password);
    bool ok = base.open();
    if(ok)
    {
        qDebug() << "База:" << nameInstance << "isConnected";
    }
    else
    {
        qDebug() << "База:" << nameInstance << "noConnected";
    }
}

int RealTimeInstance::getCountRecords(QString query)
{
    int countRecords = 0;
    if (base.isOpen())
    {
        QSqlQuery cquery(base);
        cquery.exec(query);

        while (cquery.next())
        {
            countRecords = cquery.value(0).toInt();
        }
        return countRecords;
    }
    else
    {
        qDebug() << "noConnected " << nameInstance;
        return countRecords;
    }
}

QString RealTimeInstance::getTimeReal(QString NameTable)
{
    if (base.isOpen())
    {
        QSqlQuery cquery(base);
        cquery.exec("SELECT tm FROM " + NameTable + " ORDER BY tm DESC LIMIT 1;");

        QString time_now;
        while (cquery.next())
        {
            time_now = cquery.value(0).toString();
        }
        return  time_now;
    }
    else
    {
        qDebug() << "noConnected " << nameInstance;
        return "";
    }
}

void RealTimeInstance::getStop(bool stop)
{
    this->stopsing = stop;
}
