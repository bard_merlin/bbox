#ifndef MONITORWORKER_H
#define MONITORWORKER_H

//=============================================================================================================================================
//*********************************************************************************************************************************************
//=============================================================================================================================================

#include <QObject>
#include <QThread>
#include <QSettings>
#include <QDateTime>
#include <QSqlDatabase>
#include <QDebug>
#include <QSqlQuery>

class RealTimeInstance : public QThread
{
    Q_OBJECT
public:
    RealTimeInstance(QString QueryRealTime, QString nameInstance, QSettings *settings);
    void run();
    void connectInstance();
    QString nameInstance;

signals:
    void dataout(QString nameTable, QVector<double> keyData, QList<QString> nameParams, QVector<QVector<double>> ValueData);
    void freethread();

private:
    QString RealTimeQuery;
    QDateTime currDT = QDateTime::currentDateTime();
    QSqlDatabase base;
    QString typeDB;
    QString namedb;
    QString hostname;
    int port;
    QString user;
    QString password;
    bool freeoutstatus;
    QString qformat = "yyyy-MM-dd HH:mm:ss.zzz";

    int tick = 0;
    double timememe = 0;
    bool stopsing = false;

    int getCountRecords(QString query);
    QString getTimeReal(QString NameTable);

public slots:
    void getStop(bool stop);
};

#include <QObject>
#include <QMutex>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSettings>
//================

#include "sharedresources/dataworkpaint.h"

#include "sharedresources/configdata.h"
extern ConfigData *CFGD;

#include "sharedresources/dataqto.h"
extern DataQTO *MDE;

#include <QThread>

class WorkerInstance : public QThread
{
    Q_OBJECT
public:
    WorkerInstance(QString nameInstance, QSettings *settings, void *pmon);
    ~WorkerInstance();
    void run();
    void connectInstance();
    QMutex mutex;

signals:
    void dataout(QString nameTable, QVector<double> keyData, QList<QString> nameParams, QVector<QVector<double>> ValueData);
    void freethread();
    void stepProgress(int dvalue);
    void nameWorkerWork(QString nameworker);

private:
    //only thread
    QString nameInstance;
    bool freeoutstatus;
    void *dmain;

    //for connection
    QSqlDatabase base;
    QString typeDB;
    QString namedb;
    QString hostname;
    int port;
    QString user;
    QString password;
    //mutex 
    int getCountRecords(QString query);
};

//=============================================================================================================================================
//*********************************************************************************************************************************************
//=============================================================================================================================================

#include <QDebug>
#include <QString>
#include <QQueue>
#include <QList>

#include "graphicdata/graphicchart.h"

class MonitorWorker : public QObject
{
    Q_OBJECT

public:
    struct dataCutTime
    {
        QString oftime;
        QString totime;
    };

    MonitorWorker(QString dataoftime, QString datatotime, DataWorkPaint *datawork);
    QList<DataQTO::mDataSelect> datatmpquery;
    QString oftime;
    QString totime;
    QString qformat = "yyyy-MM-dd HH:mm:ss.zzz";
    QString query = "SELECT %nameParam% FROM %nameTable% WHERE tm > TIMESTAMP '%OfTime%' AND tm < TIMESTAMP '%ToTime%'";
    QList<MonitorWorker::dataCutTime> timereformed;
    QList<MonitorWorker::dataCutTime> splitTime();
    QQueue<QString> taskQueue;
    void startMonitor();
    //mutex
    QMutex mutex;

private:
    //manageinstance
    int instanceInWorkCount;
    QList<WorkerInstance*> listworkers;

    //queue
    void formqueue();

    //outdata
    DataWorkPaint *datawork;

public: signals:
    void setRangeProgressQueue(int rangevalue);
    void setValueProgressQueue(int value);
    void stepStatus(QString step);
    void statusWorkMain(QString datareadwrite);

public slots:
    void outdata(QString nameTable, QVector<double> keyData, QList<QString> nameParams, QVector<QVector<double>> ValueData);
    void stepNowProgressQueue(int dvalue);
    void askWhatWorker(QString nameworker);

private slots:
    void freethreadaction();
};
#endif // MONITORWORKER_H
