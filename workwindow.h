#ifndef WORKWINDOW_H
#define WORKWINDOW_H

#include <QSettings>
#include <QString>
#include <QList>
#include <QWidget>
#include <QMainWindow>
#include <QAction>
#include <QApplication>
#include <QDateTime>
#include <QDateTimeEdit>
#include <QFileDialog>

#include "graphicdata/qcustomplot.h"
#include "graphicdata/build/tabbuilder.h"
#include "graphicdata/build/dbtabletree.h"
#include "hideworkarea/monitorworker.h"
#include "graphicdata/graphicchart.h"

#include "sharedresources/dataworkpaint.h"

#include "sharedresources/configdata.h"
extern ConfigData *CFGD;

#include "sharedresources/dataqto.h"
extern DataQTO *MDE;

class WorkWindow : public QMainWindow
{
    Q_OBJECT
public:
    WorkWindow();
    static int const EXIT_CODE_REBOOT;
    QMutex mutex;

private:
    QLabel *title1 = new QLabel("От: "),*title2 = new QLabel("До: ");
    QDateTimeEdit *timeof,*timeto;
    QString format = "yyyy-MM-ddTHH:mm:ss.zzz";
    QString qformat = "yyyy-MM-dd HH:mm:ss.zzz";
    QPushButton *repaintPlot, *paintPlot, *getData;
    QLineEdit *searchTreeBase;
    DBTableTree *treeBase;
    GraphicChart *chartview;
    TabBuilder *tabCentral;
    QDateTime currDT = QDateTime::currentDateTime();
    MonitorWorker *monitor = nullptr;
    void createMenuBar();
    void createStatusBar();
    void createTableItem();
    void createGraphic();
    QLabel *statusBarState;
    QLabel *whatisWork;
    QDockWidget *dockViewBase;
    QProgressBar *progress;
    QColor getRangeColor();
    QDockWidget *currentTop = nullptr;
    QPushButton *onSelected;
    QPushButton *dropSelected;

    DataWorkPaint *workPaint;

    QLabel *showCountSelected = new QLabel;
    int colselected = 0;

    bool lifeMode = false;
    QString query = "SELECT %nameParam% FROM %nameTable% WHERE tm > TIMESTAMP '%OfTime%' AND tm < TIMESTAMP '%ToTime%';";
    QList<RealTimeInstance*> RTworkerLIST;

public: signals:
    void stopwork(bool stop);

private slots:
    void eSettings();
    void eHelp();
    //void eReboot();
    void eSaveData();
    void eExit();
    void eChangeMode();

    void changeSession(QAction *action);
    void searchDataTree();
    void paintDataPlot();
    void refreshPaint();
    void onMouseMoveGraph(QMouseEvent* evt);

    void showSelectedSetting();
    void cancelSettingsShow();
    void stateItemChanged(QTreeWidgetItem *item, int column);

    //
    void outdataRT(QString nameTable, QVector<double> keyData, QList<QString> nameParams, QVector<QVector<double>> ValueData);

    void setSelectedNULL();

public slots:
    void setStatusWork(QString status);
    void setProgressWork(int gvalue);
    void setRangeProgressALL(int rangevalue);
    void stWorkMain(QString datareadwrite);

    void eFullscreenOnOff();
};

#endif // WORKWINDOW_H
