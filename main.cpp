#include "startwindow.h"
#include <QApplication>

#include "sharedresources/configdata.h"
ConfigData *CFGD;

#include "sharedresources/dataqto.h"
DataQTO *MDE;

int main(int argc, char *argv[])
{
    int currentExitCode = 0;
     do
     {
         QApplication a(argc, argv);
         StartWindow w;
         w.setWindowTitle("Черный ящик");
         w.show();
         currentExitCode = a.exec();
     }
     while (currentExitCode == WorkWindow::EXIT_CODE_REBOOT);
     return currentExitCode;
}

//https://www.citilink.ru/configurator/q25120614/
